﻿module TrainBox
{
    export class IdentifierPathPair{
        
        assetKey: string;
        path: string;

        constructor(_assetKey: string, _path: string) {
            this.assetKey = _assetKey;
            this.path = _path;
        }
    }

    //Class to handle the management and loading of assets. Assets need to be preloaded before they are added to the canvas
    export class Assets {
        //List of the asset keys paired to their reletive paths
        //Images
        public static LoadingBar: IdentifierPathPair = new TrainBox.IdentifierPathPair('loadingBar', '../TrainBox/assets/sprites/loadingBar.png');
        public static Background: IdentifierPathPair = new TrainBox.IdentifierPathPair('background', '../TrainBox/assets/sprites/BluePrintGrid.jpg');
        public static CrossHair: IdentifierPathPair = new TrainBox.IdentifierPathPair('crossHair', '../TrainBox/assets/sprites/debugSprite.png');
        public static SidePanel: IdentifierPathPair = new TrainBox.IdentifierPathPair('sidePanel', '../TrainBox/assets/sprites/SidePanel.png');
        public static PanelInsert: IdentifierPathPair = new TrainBox.IdentifierPathPair('panelInsert', '../TrainBox/assets/sprites/PanelButtonInsert.png');
        public static PanelInsertHighlight: IdentifierPathPair = new TrainBox.IdentifierPathPair('panelInsertHighlight', '../TrainBox/assets/sprites/PanelButtonInsertHighlight.png');
        public static PanelInsertGrey: IdentifierPathPair = new TrainBox.IdentifierPathPair('panelDarkGrey', '../TrainBox/assets/sprites/PanelButtonDarkGreyInsert.png');
        public static PanelInsertGreySelected: IdentifierPathPair = new TrainBox.IdentifierPathPair('panelDarkGreySelected', '../TrainBox/assets/sprites/PanelButtonDarkGreyInsertSelected.png');
        public static StraitTrack1: IdentifierPathPair = new TrainBox.IdentifierPathPair('straitTrack1', '../TrainBox/assets/sprites/Tracks/StraitTrackNormal110x110.png');
        public static CurveTrack1: IdentifierPathPair = new TrainBox.IdentifierPathPair('curveTrack1', '../TrainBox/assets/sprites/Tracks/CurveTrackNormal110x110.png');
        public static StraitTrack1Large: IdentifierPathPair = new TrainBox.IdentifierPathPair('straitTrack1Large', '../TrainBox/assets/sprites/Tracks/StraitTrackNormal150x150.png');
        public static CurveTrack1Large: IdentifierPathPair = new TrainBox.IdentifierPathPair('curveTrack1Large', '../TrainBox/assets/sprites/Tracks/CurveTrackNormal150x150.png');
        public static CrossTrack1: IdentifierPathPair = new TrainBox.IdentifierPathPair('crossTrack1', '../TrainBox/assets/sprites/Tracks/CrossTrackNormal110x110.png');
        public static CrossTrack1Large: IdentifierPathPair = new TrainBox.IdentifierPathPair('crossTrack1Large', '../TrainBox/assets/sprites/Tracks/CrossTrackNormal150x150.png');
        public static LTrackLeft: IdentifierPathPair = new TrainBox.IdentifierPathPair('lTrackLeft', '../TrainBox/assets/sprites/Tracks/LTrackLeftNormal110x110.png');
        public static LTrackLeftLarge: IdentifierPathPair = new TrainBox.IdentifierPathPair('lTrackLeftLarge', '../TrainBox/assets/sprites/Tracks/LTrackLeftNormal150x150.png');
        public static LTrackRight: IdentifierPathPair = new TrainBox.IdentifierPathPair('lTrackRight', '../TrainBox/assets/sprites/Tracks/LTrackRightNormal110x110.png');
        public static LTrackRightLarge: IdentifierPathPair = new TrainBox.IdentifierPathPair('lTrackRightLarge', '../TrainBox/assets/sprites/Tracks/LTrackRightNormal150x150.png');
        public static Train: IdentifierPathPair = new TrainBox.IdentifierPathPair('train', '../TrainBox/assets/sprites/Trains/Train.png');
        public static Carriage: IdentifierPathPair = new TrainBox.IdentifierPathPair('carriage', '../TrainBox/assets/sprites/Trains/Carriage.png');
        public static MakeTrainButton: IdentifierPathPair = new TrainBox.IdentifierPathPair('makeTrainbutton', '../TrainBox/assets/sprites/MakeTrainButton.png');
        public static Arrow: IdentifierPathPair = new TrainBox.IdentifierPathPair('arrow', '../TrainBox/assets/sprites/Arrow.png');
        public static ExplosionAnim: IdentifierPathPair = new TrainBox.IdentifierPathPair('explosion', '../TrainBox/assets/sprites/ExplosionSheet.png');
        //Audio
        public static SlideAudio: IdentifierPathPair = new TrainBox.IdentifierPathPair('slideOutAudio', '../TrainBox/assets/audio/PanelSlideOut.mp3');
        public static PlaceTrackAudio: IdentifierPathPair = new TrainBox.IdentifierPathPair('placeTrackAudio', '../TrainBox/assets/audio/PlaceTrack.mp3');
        public static ExplosionAudio: IdentifierPathPair = new TrainBox.IdentifierPathPair('explosionAudio', '../TrainBox/assets/audio/Explosion.mp3');
        public static TrainButtonAudio: IdentifierPathPair = new TrainBox.IdentifierPathPair('trainButtonAudio', '../TrainBox/assets/audio/TrainButton.mp3');

        //Load all the assets into the game, except for the loading bar. For use in the preloadedstate. Needs to be kept updated when a new asset is added
        public static LoadAllAssets(state: Phaser.State)
        {
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.Background);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.CrossHair);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.SidePanel);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.StraitTrack1);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.CurveTrack1);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.StraitTrack1Large);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.CurveTrack1Large);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.CrossTrack1);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.CrossTrack1Large);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.LTrackLeft);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.LTrackLeftLarge);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.LTrackRight);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.LTrackRightLarge);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.PanelInsert);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.PanelInsertHighlight);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.PanelInsertGrey);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.PanelInsertGreySelected);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.Train);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.Carriage);
            TrainBox.Assets.LoadSpriteSheet(state, TrainBox.Assets.MakeTrainButton, 128, 128);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.Arrow);
            TrainBox.Assets.LoadSpriteSheet(state, TrainBox.Assets.ExplosionAnim, 64, 64, 6);

            TrainBox.Assets.LoadSound(state, TrainBox.Assets.SlideAudio);
            TrainBox.Assets.LoadSound(state, TrainBox.Assets.PlaceTrackAudio);
            TrainBox.Assets.LoadSound(state, TrainBox.Assets.ExplosionAudio);
            TrainBox.Assets.LoadSound(state, TrainBox.Assets.TrainButtonAudio);
        }

        //static functions for preloading images into memory before they are stage added.
        public static LoadImage(state: Phaser.State, imageToLoad: IdentifierPathPair)
        {
            state.load.image(imageToLoad.assetKey, imageToLoad.path);
        }
        public static LoadSound(state: Phaser.State, soundToLoad: IdentifierPathPair)
        {
            state.load.audio(soundToLoad.assetKey, soundToLoad.path);
        }
        public static LoadSpriteSheet(state: Phaser.State, spriteSheetToLoad: IdentifierPathPair, frameWidth : number, frameHeight : number, frameNum : number = -1)
        {
            if (frameNum == -1) {
                state.load.spritesheet(spriteSheetToLoad.assetKey, spriteSheetToLoad.path, frameWidth, frameHeight);
            }
            else {
                state.load.spritesheet(spriteSheetToLoad.assetKey, spriteSheetToLoad.path, frameWidth, frameHeight, frameNum);
            }
        }
    }
}