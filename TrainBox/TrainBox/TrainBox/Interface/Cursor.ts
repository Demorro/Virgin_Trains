﻿module TrainBox {
    //The cursor that highlights what cell we have selected
    export class Cursor extends Phaser.Sprite {

        private currentSelectedCell: Cell;
        private gridReference: Grid;
        private selectionPanel: SelectionPanel;

        private trackSelectionGhost: Phaser.Sprite; //a sprite to render the currently selected placement track too.

        private TWEEN_TO_CELL_SPEED: number = 80;
        private isTweening: boolean = false;
        private moveTween: Phaser.Tween;

        private currentRot: number = 0; // 0-3, north to west

        private tweenComplete: Function;

        private isActive: boolean = true;

        private placeRailSound: Phaser.Sound;

        private deleteKey: Phaser.Key;

        constructor(game: Phaser.Game, startSelectingCell: Cell, gameGrid: Grid) {
            super(game, startSelectingCell.position.x, startSelectingCell.position.y, Assets.CrossHair.assetKey);
            this.currentSelectedCell = startSelectingCell;

            this.gridReference = gameGrid;

            this.tweenComplete = function(){
                this.isTweening = false;
            }

            this.trackSelectionGhost = this.game.add.sprite(0, 0);
            this.trackSelectionGhost.anchor = new Phaser.Point(0.5, 0.5);
            this.inputEnabled = true;
            this.events.onInputDown.add(this.PlaceTrack, this);
            this.events.onInputOver.add(this.PlaceTrack, this);
            this.events.onInputDown.add(this.RotateTrack, this);

            this.deleteKey = game.input.keyboard.addKey(Phaser.Keyboard.DELETE);
        }

        //Sets the reference to the selection panel, seperate to avoid initialisation ordering issues cause they both need to know about eachother
        public SetSelectionPanelReference = (_panel: SelectionPanel) => {
            this.selectionPanel = _panel;
        }

        public SnapToSelectingCell = (cell: Cell) => {
            this.position = cell.position;
            this.currentSelectedCell = cell;
        }

        public TweenToSelectingCell = (cell: Cell) => {
            this.isTweening = true;
            this.currentSelectedCell = cell;
            this.moveTween.to({ x: cell.position.x, y: cell.position.y }, this.TWEEN_TO_CELL_SPEED, Phaser.Easing.Quadratic.Out, true, 0);
            this.moveTween.onComplete.add(this.tweenComplete, this);
            this.moveTween.start();
        }

        public Enable = () => {
            this.isActive = true;
            this.visible = true;
            this.trackSelectionGhost.visible = true;
        }
        public Disable = () => {
            this.isActive = false;
            this.visible = false;
            this.trackSelectionGhost.visible = false;
        }

        public Update = (mousePos: Phaser.Point, cam :Camera) => {
            var selectedCell: Cell = this.gridReference.GetCellAtWorldPoint(new Phaser.Point(mousePos.x, mousePos.y));

            //this.DoTweenCursorMovement();
            this.SnapToSelectingCell(selectedCell);

            //If the camera is moving, don't display this sprite
            if (cam.IsScrolling()) {
                this.Disable();
            } else {
                this.Enable();
            }

            if (this.deleteKey.isDown) {
                this.EraseTrack();
            }

            this.RenderPlacementGhost();
        }

        private EraseTrack = () => {
            if (this.currentSelectedCell != null) {
                if (this.selectionPanel.GetSelectedItem() != null) {
                    //If this track can't be modified, bail out
                    if (this.currentSelectedCell.GetOccupyingTrack() != null) {
                        if (this.currentSelectedCell.GetOccupyingTrack().CanBeModified() == false) {
                            return;
                        }
                    }
                    if (this.currentSelectedCell.EraseTrack()) {
                        this.placeRailSound = this.game.sound.play(Assets.PlaceTrackAudio.assetKey);
                    }
                }
            }
        }

        //We've clicked, so place a track if we have one.
        private PlaceTrack = () => {
            if (this.game.input.activePointer.isDown) {
                if (this.game.input.activePointer.button == Phaser.Mouse.LEFT_BUTTON) {
                    if (this.currentSelectedCell != null) {
                        if (this.selectionPanel.GetSelectedItem() != null) {
                            //If this track can't be modified, bail out
                            if (this.currentSelectedCell.GetOccupyingTrack() != null) {
                                if (this.currentSelectedCell.GetOccupyingTrack().CanBeModified() == false) {
                                    return;
                                }
                            }
                            if (this.currentSelectedCell.PlaceTrack(this.selectionPanel.GetSelectedItem().track, this.currentRot)) {
                                this.placeRailSound = this.game.sound.play(Assets.PlaceTrackAudio.assetKey);
                            }
                        }
                    }
                }
            }
        }

        private RotateTrack = () => {
            if (this.game.input.activePointer.isDown) {
                if (this.game.input.activePointer.button == Phaser.Mouse.RIGHT_BUTTON) {
                    if (this.currentSelectedCell != null) {
                        if (this.selectionPanel.GetSelectedItem() != null) {
                            this.currentRot += 1.0;
                            this.currentRot = Math.floor(this.currentRot);
                            if (this.currentRot > 3) {
                                this.currentRot = 0;
                            }
                            this.trackSelectionGhost.rotation = this.currentRot * Math.PI/2;
                        }
                    }
                }
            }
        }

        //Renders the track to be placed over the cursor
        public RenderPlacementGhost = () => {
            if (this.selectionPanel != null) {
                if (this.selectionPanel.GetSelectedItem() != null) {
                    if (this.selectionPanel.GetSelectedItem().track.GetSmallKey() != this.trackSelectionGhost.key) {
                        if (this.selectionPanel.GetSelectedItem().track.GetSmallKey() != null) {
                            this.trackSelectionGhost.loadTexture(this.selectionPanel.GetSelectedItem().track.GetSmallKey());
                            this.trackSelectionGhost.alpha = 0.7;
                            this.trackSelectionGhost.visible = true;
                        }
                        else {
                            this.trackSelectionGhost.visible = false;
                        }
                    }
                    this.trackSelectionGhost.position.x = this.position.x + this.width / 2;
                    this.trackSelectionGhost.position.y = this.position.y + this.height / 2;
                }
            }
        }

        //Tweens along the grid, keeping snapped to the grid, rather than moving directly to the cursor. Pretty shitty with mouse movement, would be good for keyboard movement though, if you slowed it a bit.
        private DoTweenCursorMovement = (selectedCell: Cell) => {
            //Tween movement, not great at the moment
            if (this.isTweening == false) {
                if (selectedCell != this.currentSelectedCell) {
                    this.moveTween = this.game.add.tween(this);
                    //tween horizontal
                    if (Math.abs(selectedCell.gridIndex.x - this.currentSelectedCell.gridIndex.x) >= (Math.abs(selectedCell.gridIndex.y - this.currentSelectedCell.gridIndex.y))) {
                        if (selectedCell.gridIndex.x < this.currentSelectedCell.gridIndex.x) { //Tween left
                            this.TweenToSelectingCell(this.currentSelectedCell.GetWestCell());
                        }
                        else {
                            this.TweenToSelectingCell(this.currentSelectedCell.GetEastCell()); //Tween rigth
                        }
                    }
                    //tween vertical
                    else {
                        if (selectedCell.gridIndex.y < this.currentSelectedCell.gridIndex.y) { //Tween north
                            this.TweenToSelectingCell(this.currentSelectedCell.GetNorthCell());
                        }
                        else {
                            this.TweenToSelectingCell(this.currentSelectedCell.GetSouthCell()); //Tween south
                        }
                    }

                }
            }
        }
    }
}