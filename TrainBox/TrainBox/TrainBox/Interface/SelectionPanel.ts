﻿module TrainBox {

    //Stores the tracks that can be selected, tied to the rectangle object that defines their selection area on the panel
    export class PanelTrackSelectionItem {
        
        static PANELXOFFSET: number = 82;
        static PANELYOFFSET = 70;
        static PANELYSPACING: number = 125;

        track: Track;
        panel: Phaser.Sprite;

        isSelected: boolean = false;

        selectionPanel: SelectionPanel;

        //Pass in a track object, and the sprite that you want to be the background for it in the selection panel
        constructor(game: Phaser.Game, _track: Track, _panel: Phaser.Sprite, parentSelectionPanel: SelectionPanel) {
            this.track = _track;
            this.panel = _panel;

            this.track.fixedToCamera = true;
            this.panel.fixedToCamera = true;

            this.track.anchor = new Phaser.Point(0.5, 0.5);
            this.panel.anchor = new Phaser.Point(0.5, 0.5);

            this.panel.bringToTop();
            this.track.bringToTop();

            this.panel = game.add.existing(this.panel);
            this.track.loadTexture(this.track.GetSmallKey());
            this.track = game.add.existing(this.track);

            this.panel.inputEnabled = true;
            this.panel.events.onInputOver.add(this.OnMouseOver, this);
            this.panel.events.onInputOut.add(this.OnMouseOff, this);
            this.panel.events.onInputDown.add(this.OnMouseClick, this);

            this.selectionPanel = parentSelectionPanel;
        }

        public BringPanelToTop = () => {

            this.panel.bringToTop();
            this.track.bringToTop();
        }

        OnMouseOver = () => {
            if (this.isSelected == false) {
                this.panel.loadTexture(Assets.PanelInsertHighlight.assetKey);
            }
            else {
                this.panel.loadTexture(Assets.PanelInsertGreySelected.assetKey);
            }

        }

        OnMouseOff = () => {
            if (this.isSelected == false) {
                this.panel.loadTexture(Assets.PanelInsert.assetKey);
            }
            else {
                this.panel.loadTexture(Assets.PanelInsertGrey.assetKey);
            }
        }

        OnMouseClick = () => {
            this.selectionPanel.NotifyOfSelection(this);
            this.isSelected = true;
            this.panel.loadTexture(Assets.PanelInsertGreySelected.assetKey);
        }

        Deselect = () =>{
            this.isSelected = false;
            this.panel.loadTexture(Assets.PanelInsert.assetKey);
        }

        SetPosition = (position: Phaser.Point) => {
            console.log(position);
            this.panel.cameraOffset.x = position.x;
            this.panel.cameraOffset.y = position.y;
            this.track.cameraOffset.x = position.x;
            this.track.cameraOffset.y = position.y;
            if ((this.track.key == Assets.CurveTrack1Large.assetKey) || (this.track.key == Assets.CurveTrack1.assetKey)) {
                this.track.cameraOffset.x -= this.track.width / 8;
                this.track.cameraOffset.y -= this.track.height / 8;
            }
        }

        TweenToX = (game : Phaser.Game, position: number, tweenSpeed : number) => {
            game.add.tween(this.panel.cameraOffset).to({ x: position}, tweenSpeed, Phaser.Easing.Back.InOut, true);
            if ((this.track.key == Assets.CurveTrack1Large.assetKey) || (this.track.key == Assets.CurveTrack1.assetKey)) {
                game.add.tween(this.track.cameraOffset).to({ x: position - (this.track.width/8) }, tweenSpeed, Phaser.Easing.Back.InOut, true);
            }
            else {
                game.add.tween(this.track.cameraOffset).to({ x: position}, tweenSpeed, Phaser.Easing.Back.InOut, true);
            }
        }


    }

    //The Panel that appears on the left side of the screen. Can be slid on and of the screen by hitting the selection area
    //Allows selection of which piece is active for placement/whatever tool we have
    export class SelectionPanel extends Phaser.Sprite {

        //The button are that clicking in toggles the panel
        private selectionArea: Phaser.Rectangle;

        //Whether or not the panel is open
        private isOpen: boolean = true;

        //Length of the tween
        private tweenSpeed: number = 520;
        private isCurrentlyTweening: boolean = false;

        //At the moment the button strip is 30 pixels on the right of the sprite
        private buttonStripWidth: number = 44;
        private panelOffset: number = -190; //The X offset of the panel, as the tween| lets you see a piece of the sprite that normally sits in -x.

        private slideAudio: Phaser.Sound;

        //The button that makes a train appear
        private trainButton: Phaser.Button;
        private static TRAINBUTTONXOFFSETFROMLEFT = 19;
        private static TRAINBUTTONYFROMBOTTOM = 80;
        private static SPAWNTRAINBUTTONCOOLDOWN = 5;

        //List of track pieces that can be selected
        private selectableTracks: PanelTrackSelectionItem[];

        private selectedItem: PanelTrackSelectionItem = null; //The current selected button
        
        //A reference to the cursor, so it can be disabled if the mouse is over the selection pane
        private cursor: Cursor;

        //Can start either open or closed.
        constructor(game: Phaser.Game, startClosed: boolean, _cursor: Cursor) {
            super(game, 0, 0, Assets.SidePanel.assetKey);
            this.x += this.panelOffset;

            this.selectionArea = new Phaser.Rectangle(this.width - this.buttonStripWidth + this.panelOffset, 0, this.buttonStripWidth, this.height);
            this.cursor = _cursor;

            this.selectableTracks = [];

            this.fixedToCamera = true; //This is a UI element, so fix it to camera
            if (startClosed) {
                this.SetDirectlyClosed();
            }

            this.inputEnabled = true;
            this.cursor = _cursor;
            this.events.onInputOver.add(this.DisableCursor, this);
            this.events.onInputOut.add(this.EnableCursor, this);
   
        }

        private SpawnNewTrain = () => {
        }

        //Disable the cursor when over this panel, and enable when not
        public DisableCursor = () => {
            this.cursor.Disable();
        }
        public EnableCursor = () => {
            this.cursor.Enable();
        }

        public BringPanelToTop = () => {
            this.bringToTop();
            for (var i = 0; i < this.selectableTracks.length; i++) {
                this.selectableTracks[i].BringPanelToTop();
            }
            this.trainButton.bringToTop();
        }

        //Place the track buttons in the panel
        public PopulateSelectableTracks = (game: Phaser.Game, trainButtonCallBack: Function) => {

            this.selectableTracks.push(new PanelTrackSelectionItem(game,new Track(game, TrackType.STRAIT), new Phaser.Sprite(this.game, 0, 0, Assets.PanelInsert.assetKey),this));
            this.selectableTracks.push(new PanelTrackSelectionItem(game, new Track(game, TrackType.CORNER), new Phaser.Sprite(this.game, 0, 0, Assets.PanelInsert.assetKey), this));
            this.selectableTracks.push(new PanelTrackSelectionItem(game, new Track(game, TrackType.CROSS), new Phaser.Sprite(this.game, 0, 0, Assets.PanelInsert.assetKey), this));
            this.selectableTracks.push(new PanelTrackSelectionItem(game, new Track(game, TrackType.LRIGHT), new Phaser.Sprite(this.game, 0, 0, Assets.PanelInsert.assetKey), this));
            this.selectableTracks.push(new PanelTrackSelectionItem(game, new Track(game, TrackType.LLEFT), new Phaser.Sprite(this.game, 0, 0, Assets.PanelInsert.assetKey), this));
            this.trainButton = this.game.add.button(SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT, this.game.height - 150, Assets.MakeTrainButton.assetKey, trainButtonCallBack, this, 0, 2, 1, 3);
            this.trainButton.fixedToCamera = true;
            this.trainButton.cameraOffset.x = SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT - this.width + this.buttonStripWidth + this.trainButton.width;
            //add all the track sprites
            for (var i = 0; i < this.selectableTracks.length; i++) {
                this.selectableTracks[i].SetPosition(new Phaser.Point(PanelTrackSelectionItem.PANELXOFFSET, PanelTrackSelectionItem.PANELYOFFSET + (i * PanelTrackSelectionItem.PANELYSPACING)));  
            } 

            if (this.isOpen) {
                this.SetDirectlyOpen();
            }
            else {
                this.SetDirectlyClosed();
            }

        }

        public Update = (mousePos: Phaser.Point) => {
            if (this.game.input.mousePointer.isDown) {
                if (this.selectionArea.contains(mousePos.x, mousePos.y)) {
                    this.Toggle();
                }
            }
            
        }

        //If the panel is open, shuts it, and vice versa
        public Toggle = () => {
            if (this.isCurrentlyTweening == false) {
                //Tween panel off the left side of the screen
                if (this.isOpen) {
                    this.TweenOut();
                }
                else { //Tween panel on from the left side of the screen
                    this.TweenIn();
                }
            }
        }
        
        //Sets the panel to be directly open, without tweens or somesuch
        public SetDirectlyOpen = () => {
            this.isCurrentlyTweening = false;
            this.selectionArea.x = this.width - this.buttonStripWidth + this.panelOffset;
            this.isOpen = true;
            this.cameraOffset.x = this.panelOffset;
            //Set the panels to the open position
            for (var i = 0; i < this.selectableTracks.length; i++) {
                this.selectableTracks[i].SetPosition(new Phaser.Point(PanelTrackSelectionItem.PANELXOFFSET, PanelTrackSelectionItem.PANELYOFFSET + (i * PanelTrackSelectionItem.PANELYSPACING)));
            }  
            if (this.trainButton != null) {
                this.trainButton.cameraOffset = new Phaser.Point(SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT, this.game.height - SelectionPanel.TRAINBUTTONYFROMBOTTOM - (this.trainButton.height/2));
            }
            
        }
        //Sets the panel to be directly closed, without tweens or somesuch
        public SetDirectlyClosed = () => {
            this.isCurrentlyTweening = false;
            this.isOpen = false;
            this.selectionArea.x = 0;
            this.cameraOffset.x = -this.width + this.buttonStripWidth;
            //Set the panels to the closed position
            for (var i = 0; i < this.selectableTracks.length; i++) {
                this.selectableTracks[i].SetPosition(new Phaser.Point(PanelTrackSelectionItem.PANELXOFFSET - this.width + this.buttonStripWidth + this.selectableTracks[i].panel.width  , PanelTrackSelectionItem.PANELYOFFSET + (i * PanelTrackSelectionItem.PANELYSPACING)));
            }
            if (this.trainButton != null) {
                this.trainButton.position = new Phaser.Point(SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT - this.width + this.buttonStripWidth + this.trainButton.width, this.game.height - SelectionPanel.TRAINBUTTONYFROMBOTTOM);
            }
        }


        //Tween out left, only called internally
        private TweenOut = () => {
            this.game.add.tween(this.cameraOffset).to({ x: 0 - this.width + this.buttonStripWidth }, this.tweenSpeed, Phaser.Easing.Back.InOut, true).onComplete.add(this.TweenOutComplete, this);
            this.isCurrentlyTweening = true;
            this.slideAudio = this.game.sound.play(Assets.SlideAudio.assetKey);
            //tween the panels
            for (var i = 0; i < this.selectableTracks.length; i++) {
                this.selectableTracks[i].TweenToX(this.game, PanelTrackSelectionItem.PANELXOFFSET - this.width + this.buttonStripWidth + this.selectableTracks[i].panel.width, this.tweenSpeed);
            }

            this.game.add.tween(this.trainButton.cameraOffset).to({
                x: 0 - (this.trainButton.width*2) + this.buttonStripWidth
            }, this.tweenSpeed, Phaser.Easing.Back.InOut, true);        
        }
        //Tween In from left, only called internally
        private TweenIn = () => {
            this.game.add.tween(this.cameraOffset).to({ x: 0 + this.panelOffset }, this.tweenSpeed, Phaser.Easing.Back.InOut, true).onComplete.add(this.TweenInComplete, this);
            this.isCurrentlyTweening = true;
            this.slideAudio = this.game.sound.play(Assets.SlideAudio.assetKey);
            
            //tween the panels
            for (var i = 0; i < this.selectableTracks.length; i++) {
                this.selectableTracks[i].TweenToX(this.game, PanelTrackSelectionItem.PANELXOFFSET, this.tweenSpeed); 
            } 

            this.game.add.tween(this.trainButton.cameraOffset).to({
                x: SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT
            }, this.tweenSpeed, Phaser.Easing.Back.InOut, true);  
        }

        //Move button to closed position and set tween flag,
        private TweenOutComplete = () => {
            this.isCurrentlyTweening = false;
            this.isOpen = false;
            this.selectionArea.x = 0;
        }
        //Move button to open position and set tween flag,
        private TweenInComplete = () => {
            this.isCurrentlyTweening = false;
            this.selectionArea.x = this.width - this.buttonStripWidth + this.panelOffset;
            this.isOpen = true;
        }

        //Called from the selection objects, notifies of a button selection so all the other buttons can be de-selected
        //This notification is called first, so we can go ahead an de-select everything
        public NotifyOfSelection = (selectedButton: PanelTrackSelectionItem) => {
            for (var i = 0; i < this.selectableTracks.length; i++) {
                if (this.selectableTracks[i].isSelected) {
                    this.selectableTracks[i].Deselect();
                }
            }

            this.selectedItem = selectedButton;
        }

        public GetSelectedItem = (): PanelTrackSelectionItem => {
            return this.selectedItem
        }

        public GetItem = (index: number): PanelTrackSelectionItem => {
            index = Math.floor(index);

            if (this.selectableTracks == null) {
                return null;
            }

            if ((index < 0) || (index >= this.selectableTracks.length)) {
                return null;
            }

            return this.selectableTracks[index];
        }

        public DisableTrainSpawnButtonForTime = () => {
            this.trainButton.inputEnabled = false;
            this.trainButton.alpha = 0.5;
            this.game.time.events.add(Phaser.Timer.SECOND * SelectionPanel.SPAWNTRAINBUTTONCOOLDOWN, this.EnableTrainButton, this);
        }
        private EnableTrainButton = () => {
            this.trainButton.inputEnabled = true;
            this.trainButton.alpha = 1.0;
        }
    }
}