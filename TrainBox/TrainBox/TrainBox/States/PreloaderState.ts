﻿module TrainBox {

    export class PreloaderState extends Phaser.State {

        preloadBar: Phaser.Sprite;

        preload() {
 
            //  Set-up our preloader sprite
            this.preloadBar = this.add.sprite(200, 250, TrainBox.Assets.LoadingBar.assetKey);
            this.load.setPreloadSprite(this.preloadBar);
 
            //  Load game assets here, like this : this.load.image(TrainBox.AssetPaths.Asset.AssetKey, TrainBox.AssetPaths.Asset.AssetKey);
            TrainBox.Assets.LoadAllAssets(this);
        }

        create() {

            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startGame, this);

        }

        startGame() {
            this.game.state.start('TrainState', true, false);
        }

    }

} 