﻿module TrainBox {
    //Entry point, called from app.ts. Just for state setup
    export class Game extends Phaser.Game {

        constructor() {

            super(window.innerWidth, window.innerHeight, Phaser.AUTO, 'content', null);

            this.state.add('BootState', BootState, false);
            this.state.add('PreloaderState', PreloaderState, false);
            this.state.add('TrainState', TrainState, false);

            this.state.start('BootState');

        }

    }

}  