﻿module TrainBox {

    export class TrainState extends Phaser.State {

        background1: Phaser.Sprite;
        background2: Phaser.Sprite;
        background3: Phaser.Sprite;
        background4: Phaser.Sprite;

        constructionGrid: Grid;

        cam : Camera;

        gridSelectionCursor: Cursor; //Goes over the mouse-over cell
        selectionPanel: SelectionPanel //The panel that appears at the side, allows selection of items

        private trains: Array<Train>;
        private carriages: Array<Carraige>;
        private startTrackNotificationArrow: Phaser.Sprite;

        private gamePaused: boolean = false;

        mousePos: Phaser.Point;

        create() {
            var gridWidth: number = 36;
            var gridHeight: number = 36;
            this.constructionGrid = new Grid(this.game, gridWidth, gridHeight);

            //Set up 4 tiled backgrounds, 2x2
            this.background1 = this.add.sprite(0, 0, TrainBox.Assets.Background.assetKey);
            this.background2 = this.add.sprite(this.background1.width, 0, TrainBox.Assets.Background.assetKey);
            this.background3 = this.add.sprite(0, this.background1.height, TrainBox.Assets.Background.assetKey);
            this.background4 = this.add.sprite(this.background1.width, this.background1.height, TrainBox.Assets.Background.assetKey);
            //Set the background logic disabled for performance
            this.background1.body = null;
            this.background2.body = null;
            this.background3.body = null;
            this.background4.body = null;

            this.startTrackNotificationArrow = this.add.sprite(0, 0, TrainBox.Assets.Arrow.assetKey);

            this.cam = new Camera(this.game, Grid.CELLXPIXELS * 2, Grid.CELLYPIXELS * 2, (Grid.CELLXPIXELS * (gridWidth - 4)), Grid.CELLYPIXELS * (gridHeight - 4));
            this.gridSelectionCursor = new Cursor(this.game, this.constructionGrid.GetCellAtIndex(new Phaser.Point(5, 0)), this.constructionGrid);
            this.selectionPanel = new SelectionPanel(this.game, false, this.gridSelectionCursor);
            this.gridSelectionCursor.SetSelectionPanelReference(this.selectionPanel);
            
            this.game.add.existing(this.gridSelectionCursor);
            this.game.add.existing(this.selectionPanel);
            this.selectionPanel.PopulateSelectableTracks(this.game, this.SpawnTrain);
            this.mousePos = new Phaser.Point(0, 0);

            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 0)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);//Entry track
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 0)).GetOccupyingTrack().SetCannotBeModified(); //Dont modify the entry track   
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 1)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 1)).GetOccupyingTrack().SetCannotBeModified(); //Dont modify the entry track
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 2)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 2)).GetOccupyingTrack().SetCannotBeModified(); //Dont modify the entry track
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 3)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 3)).GetOccupyingTrack().SetCannotBeModified(); //Dont modify the entry track
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).GetOccupyingTrack().SetCannotBeModified(); //Dont modify the entry track
            this.startTrackNotificationArrow.position = new Phaser.Point(this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).position.x, this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).position.y); //Set a notification arrow;
            this.startTrackNotificationArrow.position.x -= 45;
            this.startTrackNotificationArrow.position.y += 15;

            this.trains = [];
            this.carriages = [];

            this.game.onPause.add(this.PausedGame, this);
            this.game.onResume.add(this.UnPausedGame, this);

        }

        update() {
            if (this.gamePaused == false){
                this.mousePos.x = this.input.x;
                this.mousePos.y = this.input.y;

                this.cam.Update(this.time.elapsedMS);

                this.gridSelectionCursor.Update(this.cam.ScreenToWorldSpace(this.mousePos), this.cam);
                this.selectionPanel.Update(this.mousePos);

                for (var i = 0; i < this.trains.length; i++) {

                    if (this.trains[i].alive != false) {
                        this.trains[i].DoMovement();
                    }
                    else {
                        delete this.trains[i];
                        this.trains.splice(i, 1);
                    }
                    
                }
                for (var i = 0; i < this.carriages.length; i++) {
                    if (this.carriages[i].alive != false) {
                        this.carriages[i].Update();
                    }
                    else {
                        delete this.carriages[i];
                        this.carriages.splice(i, 1);
                    }
                }

                //Check collision between trains
                for (var i = 0; i < this.trains.length; i++) {
                    for (var j = 0; j < this.trains.length; j++) {
                        if ((this.trains[i] != null) && (this.trains[j] != null)) {
                            if ((this.trains[i].alive) && (this.trains[j].alive)) {
                                if (this.trains[i].overlap(this.trains[j])) {
                                    if (this.trains[i] != this.trains[j]) {
                                        this.trains[j].CrashTrain();
                                    }
                                }
                            }
                        }
                    }
                }
                //Check collision between carriages
                for (var i = 0; i < this.carriages.length; i++) {
                    for (var j = 0; j < this.trains.length; j++) {
                        if ((this.carriages[i] != null) && (this.trains[j] != null)) {
                            if ((this.carriages[i].alive) && (this.trains[j].alive)) {
                                if (this.carriages[i].overlap(this.trains[j])) {
                                    if (this.carriages[i].belongsToTrain != this.trains[j]) {
                                        this.trains[j].CrashTrain();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private SpawnTrain = () => {
            this.game.sound.play(Assets.TrainButtonAudio.assetKey);
            this.selectionPanel.DisableTrainSpawnButtonForTime();

            var noOfCarraigesToSpawn = this.game.rnd.integerInRange(2, 8);
            const spawnOffsetY = 30;

            var startCell = this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 1));
            var testTrain = new Train(this.game, Assets.Train.assetKey, startCell, new Phaser.Point(380, startCell.position.y - spawnOffsetY));
            var lastSpawnObject: Phaser.Sprite = testTrain;
            testTrain.angle = 90;
            this.trains.push(testTrain);
            for (var i = 0; i < noOfCarraigesToSpawn; i++) {
                var testCarriage = new Carraige(this.game, Assets.Carriage.assetKey, lastSpawnObject, testTrain);
                testCarriage.position = new Phaser.Point(testTrain.position.x, testTrain.position.y);
                testCarriage.angle = 90;
                this.game.add.existing(testCarriage);
                lastSpawnObject = testCarriage;
                this.carriages.push(testCarriage);
            }
            this.game.add.existing(testTrain);
            this.selectionPanel.BringPanelToTop();
        }

        private PausedGame = () => {
            this.gamePaused = true;
        }
        private UnPausedGame = () => {
            this.gamePaused = false;
            this.game.time.elapsed = 0.166;
        }
    }
} 