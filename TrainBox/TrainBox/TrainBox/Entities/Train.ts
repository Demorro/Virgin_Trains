﻿module TrainBox {
    export class Train extends Phaser.Sprite {

        //The way the train's moving, 0-3, N-W
        private currentOrientation: number = 2;
        private trainMoveSpeed: number = 1.2;
        private targetPosition: Phaser.Point;
        private prevTargetPosition: Phaser.Point;

        private currentTweenPtsX: Array<number>;
        private currentTweenPtsY: Array<number>;
        private tweenTimer: number = 0;

        private lastPosition: Phaser.Point;

        private static DISTANCE_TILL_TRACK_CHANGE : number = 0.5; //How far from the end track point we can get before switching track

        private curCell: Cell;

        private deathExplosions: Array<Phaser.Sprite>;
        private hasCrashed: boolean = false;

        constructor(game: Phaser.Game, trainSpriteKey: string, startCell: Cell, startPos: Phaser.Point) {
            super(game, startPos.x, startPos.y, trainSpriteKey);

            this.targetPosition = startPos;
            this.prevTargetPosition = startPos;
            this.curCell = startCell;
            this.anchor = new Phaser.Point(0.5, 0.5);

            this.currentTweenPtsX = [];
            this.currentTweenPtsY = [];

            this.lastPosition = startPos;
        }

        private LoadTrackMoveTween() {
            while (this.currentTweenPtsX.length > 0) {
                this.currentTweenPtsX.pop();
            }
            while (this.currentTweenPtsY.length > 0) {
                this.currentTweenPtsY.pop();
            }

            this.prevTargetPosition = new Phaser.Point(this.targetPosition.x, this.targetPosition.y);
            this.targetPosition = this.curCell.GetEdgePoint(this.curCell.GetOccupyingTrack().GetConnectionIndex(this.ApproachingFromOrientation()));

            this.currentTweenPtsX.push(this.prevTargetPosition.x);
            this.currentTweenPtsX.push(this.curCell.position.x + Grid.CELLXPIXELS / 2);
            this.currentTweenPtsX.push(this.targetPosition.x);
            this.currentTweenPtsY.push(this.prevTargetPosition.y);
            this.currentTweenPtsY.push(this.curCell.position.y + Grid.CELLYPIXELS / 2);
            this.currentTweenPtsY.push(this.targetPosition.y);
            this.tweenTimer = 0;
        }


        public DoMovement = () => {
            if (this.curCell != null) {
                if (this.curCell.GetOccupyingTrack() != null) {
                    if (this.targetPosition == null) {
                        this.LoadTrackMoveTween();
                    }

                    //If we've reached the target point, move to the next grid
                    if (this.position.distance(this.targetPosition) < Train.DISTANCE_TILL_TRACK_CHANGE) {
                        this.currentOrientation = this.curCell.GetOccupyingTrack().GetConnectionIndex(this.ApproachingFromOrientation());
                        if (this.GetCellAlongConnection(this.currentOrientation) == null) {
                            this.CrashTrain();
                            return;
                        }
                        if (this.GetCellAlongConnection(this.currentOrientation).GetOccupyingTrack() == null) {
                            this.CrashTrain();
                            return;
                        }
                        if (this.GetCellAlongConnection(this.currentOrientation).GetOccupyingTrack().GetConnectionIndex(this.ApproachingFromOrientation()) < 0) {
                            this.CrashTrain();
                            return;
                        }
                        else {
                            this.curCell = this.GetCellAlongConnection(this.currentOrientation);
                            this.LoadTrackMoveTween();
                        }
                    }  

                    //Execute the track tween
                    var outOfPause = 0;

                    this.tweenTimer += this.trainMoveSpeed * (this.game.time.physicsElapsedMS / 1000);
                    if (this.tweenTimer > 1) {
                        this.tweenTimer = 1;
                    } 
                                
                    this.position.x = Phaser.Math.bezierInterpolation(this.currentTweenPtsX, this.tweenTimer);
                    this.position.y = Phaser.Math.bezierInterpolation(this.currentTweenPtsY, this.tweenTimer);

                    //Figure out the angle from position + last position
                    var movementVec: Phaser.Point = new Phaser.Point(this.position.y - this.lastPosition.y, this.position.x - this.lastPosition.x);
                    movementVec = movementVec.normalize();
                    this.angle = -Math.atan2(movementVec.y, movementVec.x) * (180 / Math.PI);
                    this.lastPosition.x = this.position.x;
                    this.lastPosition.y = this.position.y;
                      
                }
            }

            //Deal with death and cleaning up the explosions
            if (this.hasCrashed == true) {
                var noOfDestroyed = 0;
                if (this.deathExplosions != null) {
                    for (var i = 0; i < this.deathExplosions.length; i++) {
                        if (this.deathExplosions[i] != null) {
                            if (this.deathExplosions[i].animations.currentAnim != null) {
                                if (this.deathExplosions[i].animations.currentAnim.isFinished) {
                                    this.deathExplosions[i].destroy(true);
                                    this.game.stage.removeChild(this.deathExplosions[i]);
                                }
                            }
                            else {
                                noOfDestroyed++;
                            }
                        }
                        else {
                            noOfDestroyed++;
                        }
                    }
                    if (noOfDestroyed >= this.deathExplosions.length) {
                        console.log("Destroying train");
                        this.game.stage.removeChild(this);
                        this.destroy(true);
                        return;
                    }
                }
            }
        }

        //used for finding the next cell to go to after we've gone down this track 
        private GetCellAlongConnection = (connectionIndex : number): Cell => {
            if (connectionIndex == 0) {
                return this.curCell.GetNorthCell();
            }
            else if (connectionIndex == 1) {
                return this.curCell.GetEastCell();
            }
            else if (connectionIndex == 2) {
                return this.curCell.GetSouthCell();
            }
            else if (connectionIndex == 3) {
                return this.curCell.GetWestCell();
            }
            else {
                throw new Error("Incorrect Orientation. GetCellAlongConnection(). Train.ts");
                return null;
            }
        }

        //Essentially reverses the orientation, so we can get the approaching direction
        private ApproachingFromOrientation = () => {
            this.currentOrientation = Math.floor(this.currentOrientation);

            if (this.currentOrientation == 0) {
                return 2;
            }
            else if (this.currentOrientation == 1) {
                return 3;
            }
            else if (this.currentOrientation == 2) {
                return 0;
            }
            else if (this.currentOrientation == 3) {
                return 1;
            }
            else {
                throw new Error("Incorrect Orientation. ApproachingFromOrientation(). Train.ts");
            }
        }
        
        //Called when we hit another train or go onto a cell without a track
        public CrashTrain = () => {
            if (this.hasCrashed == false) {
                this.deathExplosions = [];
                this.hasCrashed = true;
                this.SpawnExplosion(); //Spawn one explosion so we dont just kill this thing immediately
                this.curCell = null

                var explosionOffsetMaxLength = 30;

                var explosionNo = this.game.rnd.integerInRange(4, 6);

                for (var i = 0; i < explosionNo; i++) {
                    this.game.time.events.add(Phaser.Timer.SECOND * this.game.rnd.realInRange(0.0, 0.6), this.SpawnExplosion, this);
                }

                this.game.time.events.add(Phaser.Timer.SECOND * 0.5, this.SetInvisible, this);
            }
        }

        private SpawnExplosion = () => {

            var explosionOffsetMaxLength = 32;

            var explosion = this.game.add.sprite(this.x, this.y, Assets.ExplosionAnim.assetKey);
            explosion.anchor = new Phaser.Point(0.5, 0.5);
            explosion.position.x = this.x + this.game.rnd.realInRange(-explosionOffsetMaxLength, explosionOffsetMaxLength);
            explosion.position.y = this.y + this.game.rnd.realInRange(-explosionOffsetMaxLength, explosionOffsetMaxLength);

   
            this.deathExplosions.push(explosion);
            this.deathExplosions[this.deathExplosions.length - 1].animations.add('explode');
            this.deathExplosions[this.deathExplosions.length - 1].play('explode', 12, false);

            this.game.sound.play(Assets.ExplosionAudio.assetKey);
        }

        private SetInvisible = () => {
            this.visible = false;
        }
    }
}