﻿module TrainBox {

    export enum TrackType {
        STRAIT,
        CORNER,
        CROSS,
        LRIGHT,
        LLEFT
    }

    //Represents a track piece, that can be placed on the grid. Will likely become the subclass for actual tracks
    export class Track extends Phaser.Sprite {

        private trackType: TrackType; //A key storing what type of track this is
        private trackRotation: number = 0; //Passed in when this track gets placed, 0-3, N - W

        //Define the connection points, adjusted whenever rotation is adusted, as usual, 0-3, N-W
        //For example, if northconnection is 2, that means a train approaching from north(0) will exit south (2)
        //-1 means theres no connection and the train should crash
        //The unrotated ones don't change on rotation, and arnt used for logic, but are used to derive rotation connections from
        private unrotatedNorthConnection: number = -9999;
        private unrotatedEastConnection: number = -9999;
        private unrotatedSouthConnection: number = -9999;
        private unrotatedWestConnection: number = -9999;
        //These ones are the ones we actually check, and are ajusted for rotation
        private northConnection: number;
        private eastConnection: number;
        private southConnection: number;
        private westConnection: number;

        private canBeModified: boolean = true; //Whether this track can be modified/deleted or not, for the starting track

        //Makes a track according to a type. Uses a smaller sprite if we're placing this in the world, as opposed to UI
        constructor(game: Phaser.Game, type: TrainBox.TrackType = TrainBox.TrackType.STRAIT, rotation: number = 0, beingPlacedOnGrid: boolean = false) {
            super(game, 0, 0, Assets.StraitTrack1Large.assetKey);
            this.trackType = type;
            this.trackRotation = rotation;

            this.anchor = new Phaser.Point(0.5, 0.5);

            //Set the correct sprite for the track type, this should be done in derived class constructor, this is just a stand-in for now
            if (this.trackType == TrackType.STRAIT) {
                if (beingPlacedOnGrid == false) {
                    this.loadTexture(Assets.StraitTrack1Large.assetKey);
                }
                else {
                    this.loadTexture(Assets.StraitTrack1.assetKey);
                }
                this.unrotatedNorthConnection = 2;
                this.unrotatedSouthConnection = 0;
            }
            else if (this.trackType == TrackType.CORNER) {
                if (beingPlacedOnGrid == false) {
                    this.loadTexture(Assets.CurveTrack1Large.assetKey);
                }
                else {
                    this.loadTexture(Assets.CurveTrack1.assetKey);
                }
                this.unrotatedEastConnection = 2;
                this.unrotatedSouthConnection = 1;
            }
            else if (this.trackType == TrackType.CROSS) {
                if (beingPlacedOnGrid == false) {
                    this.loadTexture(Assets.CrossTrack1Large.assetKey);
                }
                else {
                    this.loadTexture(Assets.CrossTrack1.assetKey);
                }
                this.unrotatedNorthConnection = 2;
                this.unrotatedSouthConnection = 0;
                this.unrotatedEastConnection = 3;
                this.unrotatedWestConnection = 1;
            }
            else if (this.trackType == TrackType.LRIGHT) {
                if (beingPlacedOnGrid == false) {
                    this.loadTexture(Assets.LTrackRightLarge.assetKey);
                }
                else {
                    this.loadTexture(Assets.LTrackRight.assetKey);
                }
                this.unrotatedNorthConnection = 2;
                this.unrotatedSouthConnection = 0;
                this.unrotatedEastConnection = 2;
            }
            else if (this.trackType == TrackType.LLEFT) {
                if (beingPlacedOnGrid == false) {
                    this.loadTexture(Assets.LTrackLeftLarge.assetKey);
                }
                else {
                    this.loadTexture(Assets.LTrackLeft.assetKey);
                }
                this.unrotatedNorthConnection = 2;
                this.unrotatedSouthConnection = 0;
                this.unrotatedWestConnection = 2;
            }

            this.northConnection = this.unrotatedNorthConnection;
            this.eastConnection = this.unrotatedEastConnection;
            this.southConnection = this.unrotatedSouthConnection;
            this.westConnection = this.unrotatedWestConnection;

            this.SetRotation(this.trackRotation);
        }

        //Sets the rotation of the track, takes a rotation index, (0-3) not an angle.
        public SetRotation = (rotationIndex: number) => {
            this.trackRotation = Math.floor(rotationIndex);
            if (this.trackRotation > 3) {
                this.trackRotation = 0;
            }
            this.rotation = this.trackRotation * Math.PI / 2;

            //Set the rotations of the connections
            if (rotationIndex == 0) { //North
                this.northConnection = this.unrotatedNorthConnection;
                this.eastConnection = this.unrotatedEastConnection;
                this.southConnection = this.unrotatedSouthConnection;
                this.westConnection = this.unrotatedWestConnection;
            }
            else if (rotationIndex == 1) { //East
                this.northConnection = this.unrotatedWestConnection + 1;
                this.eastConnection = this.unrotatedNorthConnection + 1;
                this.southConnection = this.unrotatedEastConnection + 1;
                this.westConnection = this.unrotatedSouthConnection + 1;
            }
            else if (rotationIndex == 2) { //South
                this.northConnection = this.unrotatedSouthConnection + 2;
                this.eastConnection = this.unrotatedWestConnection + 2;
                this.southConnection = this.unrotatedNorthConnection + 2;
                this.westConnection = this.unrotatedEastConnection + 2;
            }
            else if (rotationIndex == 3) { //West
                this.northConnection = this.unrotatedEastConnection + 3;
                this.eastConnection = this.unrotatedSouthConnection + 3;
                this.southConnection = this.unrotatedWestConnection + 3;
                this.westConnection = this.unrotatedNorthConnection + 3;
            }
            else {
                throw new Error("Incorrect Rotation Index Set. SetRotation, Track.ts");
            }

            //Overflow rotations greater than west
            this.northConnection = Math.floor(this.northConnection);
            this.eastConnection = Math.floor(this.eastConnection);
            this.southConnection = Math.floor(this.southConnection);
            this.westConnection = Math.floor(this.westConnection);
            if (this.northConnection >= 4) { this.northConnection -= 4; }
            if (this.eastConnection >= 4) { this.eastConnection -= 4; }
            if (this.southConnection >= 4) { this.southConnection -= 4; }
            if (this.westConnection >= 4) { this.westConnection -= 4; }

        }

        public GetTrackRotation = (): number => {
            return this.trackRotation;
        }

        public GetTrackType = (): TrackType => {
            return this.trackType;
        }

        //Returns what direction the track is connected too if we come at if from the approaching direction/
        public GetConnectionIndex = (approachingDirection: number): number => {
            if (approachingDirection == 0) {
                return this.northConnection;
            }
            else if (approachingDirection == 1) {
                return this.eastConnection;
            }
            else if (approachingDirection == 2) {
                return this.southConnection;
            }
            else if (approachingDirection == 3) {
                return this.westConnection;
            }
            else {
                throw new Error("Incorrect approaching direction, must be 0-3, GetConnectionIndex, Track.ts");
            }
        }

        //Returns the small asset key for this track type
        public GetSmallKey = (): string => {
            if (this.GetTrackType() == TrackType.STRAIT) {
                return Assets.StraitTrack1.assetKey;
            }
            else if (this.GetTrackType() == TrackType.CORNER) {
                return Assets.CurveTrack1.assetKey;
            }
            else if (this.GetTrackType() == TrackType.CROSS) {
                return Assets.CrossTrack1.assetKey;
            }
            else if (this.GetTrackType() == TrackType.LLEFT) {
                return Assets.LTrackLeft.assetKey;
            }
            else if (this.GetTrackType() == TrackType.LRIGHT) {
                return Assets.LTrackRight.assetKey;
            }
            else {
                return null;
            }
        }

        public SetCannotBeModified = () => {
            this.canBeModified = false;
        }
        public CanBeModified = (): boolean =>{
            return this.canBeModified;
        }
        

    }
}