﻿module TrainBox {
    //Carraige follows another sprite (probably a train, a-doy) on a time delay
    export class Carraige extends Phaser.Sprite {

        followObject: Phaser.Sprite; //The object that this carriage will be following
        followPositionBuffer: Array<Phaser.Point>; //Buffer for the positions of the leading object
        followAngleBuffer: Array<number>; //Buffer for the angle of the leading object
        followDelay: number; //The delay until we match the leading objects transform, in seconds
        grabFollowTimer: Phaser.Timer;

        updateRate: number = 16.6666; //In MS

        private deathExplosions: Array<Phaser.Sprite>;
        private hasCrashed: boolean = false;

        public belongsToTrain: Train

        constructor(game: Phaser.Game, carriageSpriteKey: string, followObject: Phaser.Sprite, belongsToTrain: Train, followDelay : number = (16.6666/1000) * 35) {
            super(game, 0, 0, carriageSpriteKey);

            this.followObject = followObject;
            this.position = new Phaser.Point(this.position.x, this.position.y);

            this.followPositionBuffer = [];
            this.followAngleBuffer = [];

            this.belongsToTrain = belongsToTrain;

            //Set up the timer to grab the transform at a fixed time of 60fps
           // this.grabFollowTimer = new Phaser.Timer(game, false);
           // this.grabFollowTimer.loop(this.updateRate, this.GetFollowTransform);
          //  game.time.events.loop(this.updateRate, this.GetFollowTransform);

            this.followDelay = followDelay

            this.anchor = new Phaser.Point(0.5, 0.5);
        }

        private GetFollowTransform = () => {
            this.followPositionBuffer.push(new Phaser.Point(this.followObject.position.x, this.followObject.position.y));
            this.followAngleBuffer.push(this.followObject.angle);
        }

        public Update()
        {
            if (this.followObject.visible == true) {
                this.GetFollowTransform();

                if ((this.followAngleBuffer.length * (this.updateRate / 1000)) >= this.followDelay) //If the buffer is longer than the delay, the we update positions
                {
                    this.followAngleBuffer.reverse();
                    this.angle = this.followAngleBuffer.pop();
                    this.followAngleBuffer.reverse();

                    this.followPositionBuffer.reverse();
                    var newFollowPos = this.followPositionBuffer.pop();
                    this.position = new Phaser.Point(newFollowPos.x, newFollowPos.y);
                    this.followPositionBuffer.reverse();
                }
            }
            else {
                if (this.hasCrashed == false) {
                    this.hasCrashed = true;
                    this.CrashCarriage();
                }
                //Deal with death and cleaning up the explosions
                if (this.hasCrashed == true) {
                    var noOfDestroyed = 0;
                    if (this.deathExplosions != null) {
                        for (var i = 0; i < this.deathExplosions.length; i++) {
                            if (this.deathExplosions[i] != null) {
                                if (this.deathExplosions[i].animations.currentAnim != null) {
                                    if (this.deathExplosions[i].animations.currentAnim.isFinished) {
                                        this.deathExplosions[i].destroy(true);
                                        this.game.stage.removeChild(this.deathExplosions[i]);
                                    }
                                }
                                else {
                                    noOfDestroyed++;
                                }
                            }
                            else {
                                noOfDestroyed++;
                            }
                        }
                        if (noOfDestroyed >= this.deathExplosions.length) {
                            this.game.stage.removeChild(this);
                            this.destroy(true);
                            return;
                        }
                    }
                }
            }
        }

        //Called when we hit another train or go onto a cell without a track
        private CrashCarriage = () => {
            this.deathExplosions = [];
            this.hasCrashed = true;
            this.SpawnExplosion(); //Spawn one explosion so we dont just kill this thing immediately

            var explosionOffsetMaxLength = 30;

            var explosionNo = this.game.rnd.integerInRange(4, 6);

            for (var i = 0; i < explosionNo; i++) {
                this.game.time.events.add(Phaser.Timer.SECOND * this.game.rnd.realInRange(0.0, 0.6), this.SpawnExplosion, this);
            }

            this.game.time.events.add(Phaser.Timer.SECOND * 0.5, this.SetInvisible, this);

        }

        private SpawnExplosion = () =>
        {
            var explosionOffsetMaxLength = 32;

            var explosion = this.game.add.sprite(this.x, this.y, Assets.ExplosionAnim.assetKey);
            
            explosion.anchor = new Phaser.Point(0.5, 0.5);
            explosion.position.x = this.x + this.game.rnd.realInRange(-explosionOffsetMaxLength, explosionOffsetMaxLength);
            explosion.position.y = this.y + this.game.rnd.realInRange(-explosionOffsetMaxLength, explosionOffsetMaxLength);

   
            this.deathExplosions.push(explosion);
            this.deathExplosions[this.deathExplosions.length - 1].animations.add('explode');
            this.deathExplosions[this.deathExplosions.length - 1].play('explode', 12, false);

            this.game.sound.play(Assets.ExplosionAudio.assetKey);
        }

        private SetInvisible = () => {
            this.visible = false;
        }
    }
}
