﻿module TrainBox {
    //The square grid, contains an array of cells and accesor methods for um ... accesing them.
    export class Grid {

        private cells: Cell[][];

        //The width/height of the cells, in pixels (hopefully).
        public static CELLXPIXELS = 110;
        public static CELLYPIXELS = 110;

        constructor(game: Phaser.Game, xGridWidth: number, yGridWidth: number) {
            this.PopulateGridWithCells(game, xGridWidth, yGridWidth);
        }

        //Called from constructor, so we can bind 'this' reference properly
        private PopulateGridWithCells = (game: Phaser.Game, xGridWidth: number, yGridWidth: number) => {

            this.cells = [];

            //The provided x and y numbers are rounded so they're ints, as javascript dosen't "do" ints, the high maintenance bitch.
            xGridWidth = Math.round(xGridWidth);
            yGridWidth = Math.round(yGridWidth);

            //Shove the cells in, creating Y-arrays as we go. Cells are given reference to this, and we use the CELLX/YPIXEL constants to hook up cell positions
            for (var i = 0; i < xGridWidth; i++) {
                this.cells[i] = [];
                for (var j = 0; j < yGridWidth; j++) {
                    var newCell: Cell = new Cell(game, this, new Phaser.Point(i, j), new Phaser.Point(i * Grid.CELLXPIXELS, j * Grid.CELLYPIXELS));
                    this.cells[i].push(newCell);
                }
            }
        }

        //Directly access cell at specified index
        public GetCellAtIndex  = (index: Phaser.Point): Cell => {
            //Round numbers to ints
            index = index.floor();

            if (this.cells.length > index.x) {
                if (this.cells[index.x].length > index.y) {
                    return this.cells[index.x][index.y];
                }
            }

            //If we got here, we tried to access out of array bounds, and should error.
            //throw new Error("Attempting To Access out of range Cell. Grid.ts, GetCellAtIndex()");
            return null;
        }

        //Access the cell under a world position, most useful for getting a cell at the mouse position I'd imagine, although if theres camera translation screen->worldspace will need to be done.
        public GetCellAtWorldPoint = (worldPosition: Phaser.Point) => {
            //Derive x/y indices from world positions
            var xIndex : number = worldPosition.x / Grid.CELLXPIXELS;
            var yIndex : number = worldPosition.y / Grid.CELLYPIXELS;

            //Round the indices down.
            xIndex = Math.floor(xIndex);
            yIndex = Math.floor(yIndex);

            if (this.cells.length > xIndex) {
                if (this.cells[xIndex].length > yIndex) {
                    return this.cells[xIndex][yIndex];
                }
            }
            
            //If we got here, we tried to access out of array bounds, and should error.
            throw new Error("Attempting To Access out of range Cell. Grid.ts, GetCellAtWorldPoint()");
            return null;
        }
    }
}