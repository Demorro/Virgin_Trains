﻿module TrainBox {
    //A single cell on the grid, can hold a reference to a track.
    export class Cell {

        parentGrid: Grid; //The grid that this cell is contained in.
        gridIndex: Phaser.Point; //The index of this Cell in its parent grid container
        position: Phaser.Point; //The position of the center of this cell in world space

        game: Phaser.Game; //Reference to the game;

        occupyingTrack: Track = null;

        constructor(_game: Phaser.Game, _parentGrid: Grid, _gridIndex: Phaser.Point, _worldPosition: Phaser.Point) {
            this.parentGrid = _parentGrid;
            this.gridIndex = _gridIndex;
            this.position = _worldPosition;
            this.game = _game;
        }

        public GetNorthCell = (): Cell => {
            return this.parentGrid.GetCellAtIndex(new Phaser.Point(this.gridIndex.x, this.gridIndex.y - 1));
        }
        public GetEastCell = (): Cell => {
            return this.parentGrid.GetCellAtIndex(new Phaser.Point(this.gridIndex.x + 1, this.gridIndex.y));
        }
        public GetSouthCell = (): Cell => {
            return this.parentGrid.GetCellAtIndex(new Phaser.Point(this.gridIndex.x, this.gridIndex.y + 1));
        }
        public GetWestCell = (): Cell => {
            return this.parentGrid.GetCellAtIndex(new Phaser.Point(this.gridIndex.x - 1, this.gridIndex.y));
        }

        //Returns true if we've placed a track
        public PlaceTrack = (track: Track, trackRotIndex : number): boolean => {

            // returns out if the placement is unnescesary
            if(this.occupyingTrack != null){
                if (this.occupyingTrack.GetTrackType() == track.GetTrackType()) {
                    if (this.occupyingTrack.GetTrackRotation() == trackRotIndex) {
                        return false;
                    }
                }
            }

            if (this.occupyingTrack != null) {
                this.game.stage.removeChild(this.occupyingTrack);
                this.occupyingTrack.destroy();
                this.occupyingTrack = null;
            }
            var trackToAdd: Track = new Track(this.game, track.GetTrackType(), trackRotIndex, true);

            this.occupyingTrack = this.game.world.addAt(trackToAdd, 5);
            this.occupyingTrack.x = this.position.x + Grid.CELLXPIXELS / 2;
            this.occupyingTrack.y = this.position.y + Grid.CELLYPIXELS / 2;
            return true;
        }
        //returns true if we've deleted a track
        public EraseTrack = (): boolean => {
            console.log('erasing');
            // returns out if the erasing is unnescesary
            if (this.occupyingTrack == null) {
                        return false;
            }

            console.log("Erasinghere");

            if (this.occupyingTrack != null) {
                this.game.stage.removeChild(this.occupyingTrack);
                this.occupyingTrack.destroy();
                this.occupyingTrack = null;
            }
            return true;
        }

        public GetOccupyingTrack = (): Track => {
            return this.occupyingTrack;
        }

        //Returns the position in the center of one edge, as defined by direction (0-3, N-W)
        public GetEdgePoint = (direction: number): Phaser.Point => {
            direction = Math.floor(direction);
            if (direction < 0) {
                direction = 0;
            }
            if (direction > 3) {
                direction = 3;
            }

            if (direction == 0) {
                return new Phaser.Point(this.position.x + Grid.CELLXPIXELS/2, this.position.y);
            }
            else if (direction == 1) {
                return new Phaser.Point(this.position.x + Grid.CELLXPIXELS, this.position.y + Grid.CELLYPIXELS/2);
            }
            else if (direction == 2) {
                return new Phaser.Point(this.position.x + Grid.CELLXPIXELS/2, this.position.y + Grid.CELLYPIXELS);
            }
            else if (direction == 3) {
                return new Phaser.Point(this.position.x, this.position.y + Grid.CELLYPIXELS/2);
            }
            else {
                throw new Error("Incorrect Direction. GetEdgePoint(), Cell.ts");
            }
        }
    }
}