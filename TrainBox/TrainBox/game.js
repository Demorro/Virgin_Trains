window.onload = function () {
    var game = new TrainBox.Game();
};
var TrainBox;
(function (TrainBox) {
    var IdentifierPathPair = (function () {
        function IdentifierPathPair(_assetKey, _path) {
            this.assetKey = _assetKey;
            this.path = _path;
        }
        return IdentifierPathPair;
    })();
    TrainBox.IdentifierPathPair = IdentifierPathPair;
    //Class to handle the management and loading of assets. Assets need to be preloaded before they are added to the canvas
    var Assets = (function () {
        function Assets() {
        }
        //Load all the assets into the game, except for the loading bar. For use in the preloadedstate. Needs to be kept updated when a new asset is added
        Assets.LoadAllAssets = function (state) {
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.Background);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.CrossHair);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.SidePanel);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.StraitTrack1);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.CurveTrack1);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.StraitTrack1Large);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.CurveTrack1Large);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.CrossTrack1);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.CrossTrack1Large);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.LTrackLeft);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.LTrackLeftLarge);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.LTrackRight);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.LTrackRightLarge);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.PanelInsert);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.PanelInsertHighlight);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.PanelInsertGrey);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.PanelInsertGreySelected);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.Train);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.Carriage);
            TrainBox.Assets.LoadSpriteSheet(state, TrainBox.Assets.MakeTrainButton, 128, 128);
            TrainBox.Assets.LoadImage(state, TrainBox.Assets.Arrow);
            TrainBox.Assets.LoadSpriteSheet(state, TrainBox.Assets.ExplosionAnim, 64, 64, 6);
            TrainBox.Assets.LoadSound(state, TrainBox.Assets.SlideAudio);
            TrainBox.Assets.LoadSound(state, TrainBox.Assets.PlaceTrackAudio);
            TrainBox.Assets.LoadSound(state, TrainBox.Assets.ExplosionAudio);
            TrainBox.Assets.LoadSound(state, TrainBox.Assets.TrainButtonAudio);
        };
        //static functions for preloading images into memory before they are stage added.
        Assets.LoadImage = function (state, imageToLoad) {
            state.load.image(imageToLoad.assetKey, imageToLoad.path);
        };
        Assets.LoadSound = function (state, soundToLoad) {
            state.load.audio(soundToLoad.assetKey, soundToLoad.path);
        };
        Assets.LoadSpriteSheet = function (state, spriteSheetToLoad, frameWidth, frameHeight, frameNum) {
            if (frameNum === void 0) { frameNum = -1; }
            if (frameNum == -1) {
                state.load.spritesheet(spriteSheetToLoad.assetKey, spriteSheetToLoad.path, frameWidth, frameHeight);
            }
            else {
                state.load.spritesheet(spriteSheetToLoad.assetKey, spriteSheetToLoad.path, frameWidth, frameHeight, frameNum);
            }
        };
        //List of the asset keys paired to their reletive paths
        //Images
        Assets.LoadingBar = new TrainBox.IdentifierPathPair('loadingBar', '../TrainBox/assets/sprites/loadingBar.png');
        Assets.Background = new TrainBox.IdentifierPathPair('background', '../TrainBox/assets/sprites/BluePrintGrid.jpg');
        Assets.CrossHair = new TrainBox.IdentifierPathPair('crossHair', '../TrainBox/assets/sprites/debugSprite.png');
        Assets.SidePanel = new TrainBox.IdentifierPathPair('sidePanel', '../TrainBox/assets/sprites/SidePanel.png');
        Assets.PanelInsert = new TrainBox.IdentifierPathPair('panelInsert', '../TrainBox/assets/sprites/PanelButtonInsert.png');
        Assets.PanelInsertHighlight = new TrainBox.IdentifierPathPair('panelInsertHighlight', '../TrainBox/assets/sprites/PanelButtonInsertHighlight.png');
        Assets.PanelInsertGrey = new TrainBox.IdentifierPathPair('panelDarkGrey', '../TrainBox/assets/sprites/PanelButtonDarkGreyInsert.png');
        Assets.PanelInsertGreySelected = new TrainBox.IdentifierPathPair('panelDarkGreySelected', '../TrainBox/assets/sprites/PanelButtonDarkGreyInsertSelected.png');
        Assets.StraitTrack1 = new TrainBox.IdentifierPathPair('straitTrack1', '../TrainBox/assets/sprites/Tracks/StraitTrackNormal110x110.png');
        Assets.CurveTrack1 = new TrainBox.IdentifierPathPair('curveTrack1', '../TrainBox/assets/sprites/Tracks/CurveTrackNormal110x110.png');
        Assets.StraitTrack1Large = new TrainBox.IdentifierPathPair('straitTrack1Large', '../TrainBox/assets/sprites/Tracks/StraitTrackNormal150x150.png');
        Assets.CurveTrack1Large = new TrainBox.IdentifierPathPair('curveTrack1Large', '../TrainBox/assets/sprites/Tracks/CurveTrackNormal150x150.png');
        Assets.CrossTrack1 = new TrainBox.IdentifierPathPair('crossTrack1', '../TrainBox/assets/sprites/Tracks/CrossTrackNormal110x110.png');
        Assets.CrossTrack1Large = new TrainBox.IdentifierPathPair('crossTrack1Large', '../TrainBox/assets/sprites/Tracks/CrossTrackNormal150x150.png');
        Assets.LTrackLeft = new TrainBox.IdentifierPathPair('lTrackLeft', '../TrainBox/assets/sprites/Tracks/LTrackLeftNormal110x110.png');
        Assets.LTrackLeftLarge = new TrainBox.IdentifierPathPair('lTrackLeftLarge', '../TrainBox/assets/sprites/Tracks/LTrackLeftNormal150x150.png');
        Assets.LTrackRight = new TrainBox.IdentifierPathPair('lTrackRight', '../TrainBox/assets/sprites/Tracks/LTrackRightNormal110x110.png');
        Assets.LTrackRightLarge = new TrainBox.IdentifierPathPair('lTrackRightLarge', '../TrainBox/assets/sprites/Tracks/LTrackRightNormal150x150.png');
        Assets.Train = new TrainBox.IdentifierPathPair('train', '../TrainBox/assets/sprites/Trains/Train.png');
        Assets.Carriage = new TrainBox.IdentifierPathPair('carriage', '../TrainBox/assets/sprites/Trains/Carriage.png');
        Assets.MakeTrainButton = new TrainBox.IdentifierPathPair('makeTrainbutton', '../TrainBox/assets/sprites/MakeTrainButton.png');
        Assets.Arrow = new TrainBox.IdentifierPathPair('arrow', '../TrainBox/assets/sprites/Arrow.png');
        Assets.ExplosionAnim = new TrainBox.IdentifierPathPair('explosion', '../TrainBox/assets/sprites/ExplosionSheet.png');
        //Audio
        Assets.SlideAudio = new TrainBox.IdentifierPathPair('slideOutAudio', '../TrainBox/assets/audio/PanelSlideOut.mp3');
        Assets.PlaceTrackAudio = new TrainBox.IdentifierPathPair('placeTrackAudio', '../TrainBox/assets/audio/PlaceTrack.mp3');
        Assets.ExplosionAudio = new TrainBox.IdentifierPathPair('explosionAudio', '../TrainBox/assets/audio/Explosion.mp3');
        Assets.TrainButtonAudio = new TrainBox.IdentifierPathPair('trainButtonAudio', '../TrainBox/assets/audio/TrainButton.mp3');
        return Assets;
    })();
    TrainBox.Assets = Assets;
})(TrainBox || (TrainBox = {}));
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TrainBox;
(function (TrainBox) {
    //Carraige follows another sprite (probably a train, a-doy) on a time delay
    var Carraige = (function (_super) {
        __extends(Carraige, _super);
        function Carraige(game, carriageSpriteKey, followObject, belongsToTrain, followDelay) {
            var _this = this;
            if (followDelay === void 0) { followDelay = (16.6666 / 1000) * 35; }
            _super.call(this, game, 0, 0, carriageSpriteKey);
            this.updateRate = 16.6666; //In MS
            this.hasCrashed = false;
            this.GetFollowTransform = function () {
                _this.followPositionBuffer.push(new Phaser.Point(_this.followObject.position.x, _this.followObject.position.y));
                _this.followAngleBuffer.push(_this.followObject.angle);
            };
            //Called when we hit another train or go onto a cell without a track
            this.CrashCarriage = function () {
                _this.deathExplosions = [];
                _this.hasCrashed = true;
                _this.SpawnExplosion(); //Spawn one explosion so we dont just kill this thing immediately
                var explosionOffsetMaxLength = 30;
                var explosionNo = _this.game.rnd.integerInRange(4, 6);
                for (var i = 0; i < explosionNo; i++) {
                    _this.game.time.events.add(Phaser.Timer.SECOND * _this.game.rnd.realInRange(0.0, 0.6), _this.SpawnExplosion, _this);
                }
                _this.game.time.events.add(Phaser.Timer.SECOND * 0.5, _this.SetInvisible, _this);
            };
            this.SpawnExplosion = function () {
                var explosionOffsetMaxLength = 32;
                var explosion = _this.game.add.sprite(_this.x, _this.y, TrainBox.Assets.ExplosionAnim.assetKey);
                explosion.anchor = new Phaser.Point(0.5, 0.5);
                explosion.position.x = _this.x + _this.game.rnd.realInRange(-explosionOffsetMaxLength, explosionOffsetMaxLength);
                explosion.position.y = _this.y + _this.game.rnd.realInRange(-explosionOffsetMaxLength, explosionOffsetMaxLength);
                _this.deathExplosions.push(explosion);
                _this.deathExplosions[_this.deathExplosions.length - 1].animations.add('explode');
                _this.deathExplosions[_this.deathExplosions.length - 1].play('explode', 12, false);
                _this.game.sound.play(TrainBox.Assets.ExplosionAudio.assetKey);
            };
            this.SetInvisible = function () {
                _this.visible = false;
            };
            this.followObject = followObject;
            this.position = new Phaser.Point(this.position.x, this.position.y);
            this.followPositionBuffer = [];
            this.followAngleBuffer = [];
            this.belongsToTrain = belongsToTrain;
            //Set up the timer to grab the transform at a fixed time of 60fps
            // this.grabFollowTimer = new Phaser.Timer(game, false);
            // this.grabFollowTimer.loop(this.updateRate, this.GetFollowTransform);
            //  game.time.events.loop(this.updateRate, this.GetFollowTransform);
            this.followDelay = followDelay;
            this.anchor = new Phaser.Point(0.5, 0.5);
        }
        Carraige.prototype.Update = function () {
            if (this.followObject.visible == true) {
                this.GetFollowTransform();
                if ((this.followAngleBuffer.length * (this.updateRate / 1000)) >= this.followDelay) {
                    this.followAngleBuffer.reverse();
                    this.angle = this.followAngleBuffer.pop();
                    this.followAngleBuffer.reverse();
                    this.followPositionBuffer.reverse();
                    var newFollowPos = this.followPositionBuffer.pop();
                    this.position = new Phaser.Point(newFollowPos.x, newFollowPos.y);
                    this.followPositionBuffer.reverse();
                }
            }
            else {
                if (this.hasCrashed == false) {
                    this.hasCrashed = true;
                    this.CrashCarriage();
                }
                //Deal with death and cleaning up the explosions
                if (this.hasCrashed == true) {
                    var noOfDestroyed = 0;
                    if (this.deathExplosions != null) {
                        for (var i = 0; i < this.deathExplosions.length; i++) {
                            if (this.deathExplosions[i] != null) {
                                if (this.deathExplosions[i].animations.currentAnim != null) {
                                    if (this.deathExplosions[i].animations.currentAnim.isFinished) {
                                        this.deathExplosions[i].destroy(true);
                                        this.game.stage.removeChild(this.deathExplosions[i]);
                                    }
                                }
                                else {
                                    noOfDestroyed++;
                                }
                            }
                            else {
                                noOfDestroyed++;
                            }
                        }
                        if (noOfDestroyed >= this.deathExplosions.length) {
                            this.game.stage.removeChild(this);
                            this.destroy(true);
                            return;
                        }
                    }
                }
            }
        };
        return Carraige;
    })(Phaser.Sprite);
    TrainBox.Carraige = Carraige;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    (function (TrackType) {
        TrackType[TrackType["STRAIT"] = 0] = "STRAIT";
        TrackType[TrackType["CORNER"] = 1] = "CORNER";
        TrackType[TrackType["CROSS"] = 2] = "CROSS";
        TrackType[TrackType["LRIGHT"] = 3] = "LRIGHT";
        TrackType[TrackType["LLEFT"] = 4] = "LLEFT";
    })(TrainBox.TrackType || (TrainBox.TrackType = {}));
    var TrackType = TrainBox.TrackType;
    //Represents a track piece, that can be placed on the grid. Will likely become the subclass for actual tracks
    var Track = (function (_super) {
        __extends(Track, _super);
        //Makes a track according to a type. Uses a smaller sprite if we're placing this in the world, as opposed to UI
        function Track(game, type, rotation, beingPlacedOnGrid) {
            var _this = this;
            if (type === void 0) { type = TrainBox.TrackType.STRAIT; }
            if (rotation === void 0) { rotation = 0; }
            if (beingPlacedOnGrid === void 0) { beingPlacedOnGrid = false; }
            _super.call(this, game, 0, 0, TrainBox.Assets.StraitTrack1Large.assetKey);
            this.trackRotation = 0; //Passed in when this track gets placed, 0-3, N - W
            //Define the connection points, adjusted whenever rotation is adusted, as usual, 0-3, N-W
            //For example, if northconnection is 2, that means a train approaching from north(0) will exit south (2)
            //-1 means theres no connection and the train should crash
            //The unrotated ones don't change on rotation, and arnt used for logic, but are used to derive rotation connections from
            this.unrotatedNorthConnection = -9999;
            this.unrotatedEastConnection = -9999;
            this.unrotatedSouthConnection = -9999;
            this.unrotatedWestConnection = -9999;
            this.canBeModified = true; //Whether this track can be modified/deleted or not, for the starting track
            //Sets the rotation of the track, takes a rotation index, (0-3) not an angle.
            this.SetRotation = function (rotationIndex) {
                _this.trackRotation = Math.floor(rotationIndex);
                if (_this.trackRotation > 3) {
                    _this.trackRotation = 0;
                }
                _this.rotation = _this.trackRotation * Math.PI / 2;
                //Set the rotations of the connections
                if (rotationIndex == 0) {
                    _this.northConnection = _this.unrotatedNorthConnection;
                    _this.eastConnection = _this.unrotatedEastConnection;
                    _this.southConnection = _this.unrotatedSouthConnection;
                    _this.westConnection = _this.unrotatedWestConnection;
                }
                else if (rotationIndex == 1) {
                    _this.northConnection = _this.unrotatedWestConnection + 1;
                    _this.eastConnection = _this.unrotatedNorthConnection + 1;
                    _this.southConnection = _this.unrotatedEastConnection + 1;
                    _this.westConnection = _this.unrotatedSouthConnection + 1;
                }
                else if (rotationIndex == 2) {
                    _this.northConnection = _this.unrotatedSouthConnection + 2;
                    _this.eastConnection = _this.unrotatedWestConnection + 2;
                    _this.southConnection = _this.unrotatedNorthConnection + 2;
                    _this.westConnection = _this.unrotatedEastConnection + 2;
                }
                else if (rotationIndex == 3) {
                    _this.northConnection = _this.unrotatedEastConnection + 3;
                    _this.eastConnection = _this.unrotatedSouthConnection + 3;
                    _this.southConnection = _this.unrotatedWestConnection + 3;
                    _this.westConnection = _this.unrotatedNorthConnection + 3;
                }
                else {
                    throw new Error("Incorrect Rotation Index Set. SetRotation, Track.ts");
                }
                //Overflow rotations greater than west
                _this.northConnection = Math.floor(_this.northConnection);
                _this.eastConnection = Math.floor(_this.eastConnection);
                _this.southConnection = Math.floor(_this.southConnection);
                _this.westConnection = Math.floor(_this.westConnection);
                if (_this.northConnection >= 4) {
                    _this.northConnection -= 4;
                }
                if (_this.eastConnection >= 4) {
                    _this.eastConnection -= 4;
                }
                if (_this.southConnection >= 4) {
                    _this.southConnection -= 4;
                }
                if (_this.westConnection >= 4) {
                    _this.westConnection -= 4;
                }
            };
            this.GetTrackRotation = function () {
                return _this.trackRotation;
            };
            this.GetTrackType = function () {
                return _this.trackType;
            };
            //Returns what direction the track is connected too if we come at if from the approaching direction/
            this.GetConnectionIndex = function (approachingDirection) {
                if (approachingDirection == 0) {
                    return _this.northConnection;
                }
                else if (approachingDirection == 1) {
                    return _this.eastConnection;
                }
                else if (approachingDirection == 2) {
                    return _this.southConnection;
                }
                else if (approachingDirection == 3) {
                    return _this.westConnection;
                }
                else {
                    throw new Error("Incorrect approaching direction, must be 0-3, GetConnectionIndex, Track.ts");
                }
            };
            //Returns the small asset key for this track type
            this.GetSmallKey = function () {
                if (_this.GetTrackType() == TrackType.STRAIT) {
                    return TrainBox.Assets.StraitTrack1.assetKey;
                }
                else if (_this.GetTrackType() == TrackType.CORNER) {
                    return TrainBox.Assets.CurveTrack1.assetKey;
                }
                else if (_this.GetTrackType() == TrackType.CROSS) {
                    return TrainBox.Assets.CrossTrack1.assetKey;
                }
                else if (_this.GetTrackType() == TrackType.LLEFT) {
                    return TrainBox.Assets.LTrackLeft.assetKey;
                }
                else if (_this.GetTrackType() == TrackType.LRIGHT) {
                    return TrainBox.Assets.LTrackRight.assetKey;
                }
                else {
                    return null;
                }
            };
            this.SetCannotBeModified = function () {
                _this.canBeModified = false;
            };
            this.CanBeModified = function () {
                return _this.canBeModified;
            };
            this.trackType = type;
            this.trackRotation = rotation;
            this.anchor = new Phaser.Point(0.5, 0.5);
            //Set the correct sprite for the track type, this should be done in derived class constructor, this is just a stand-in for now
            if (this.trackType == TrackType.STRAIT) {
                if (beingPlacedOnGrid == false) {
                    this.loadTexture(TrainBox.Assets.StraitTrack1Large.assetKey);
                }
                else {
                    this.loadTexture(TrainBox.Assets.StraitTrack1.assetKey);
                }
                this.unrotatedNorthConnection = 2;
                this.unrotatedSouthConnection = 0;
            }
            else if (this.trackType == TrackType.CORNER) {
                if (beingPlacedOnGrid == false) {
                    this.loadTexture(TrainBox.Assets.CurveTrack1Large.assetKey);
                }
                else {
                    this.loadTexture(TrainBox.Assets.CurveTrack1.assetKey);
                }
                this.unrotatedEastConnection = 2;
                this.unrotatedSouthConnection = 1;
            }
            else if (this.trackType == TrackType.CROSS) {
                if (beingPlacedOnGrid == false) {
                    this.loadTexture(TrainBox.Assets.CrossTrack1Large.assetKey);
                }
                else {
                    this.loadTexture(TrainBox.Assets.CrossTrack1.assetKey);
                }
                this.unrotatedNorthConnection = 2;
                this.unrotatedSouthConnection = 0;
                this.unrotatedEastConnection = 3;
                this.unrotatedWestConnection = 1;
            }
            else if (this.trackType == TrackType.LRIGHT) {
                if (beingPlacedOnGrid == false) {
                    this.loadTexture(TrainBox.Assets.LTrackRightLarge.assetKey);
                }
                else {
                    this.loadTexture(TrainBox.Assets.LTrackRight.assetKey);
                }
                this.unrotatedNorthConnection = 2;
                this.unrotatedSouthConnection = 0;
                this.unrotatedEastConnection = 2;
            }
            else if (this.trackType == TrackType.LLEFT) {
                if (beingPlacedOnGrid == false) {
                    this.loadTexture(TrainBox.Assets.LTrackLeftLarge.assetKey);
                }
                else {
                    this.loadTexture(TrainBox.Assets.LTrackLeft.assetKey);
                }
                this.unrotatedNorthConnection = 2;
                this.unrotatedSouthConnection = 0;
                this.unrotatedWestConnection = 2;
            }
            this.northConnection = this.unrotatedNorthConnection;
            this.eastConnection = this.unrotatedEastConnection;
            this.southConnection = this.unrotatedSouthConnection;
            this.westConnection = this.unrotatedWestConnection;
            this.SetRotation(this.trackRotation);
        }
        return Track;
    })(Phaser.Sprite);
    TrainBox.Track = Track;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    var Train = (function (_super) {
        __extends(Train, _super);
        function Train(game, trainSpriteKey, startCell, startPos) {
            var _this = this;
            _super.call(this, game, startPos.x, startPos.y, trainSpriteKey);
            //The way the train's moving, 0-3, N-W
            this.currentOrientation = 2;
            this.trainMoveSpeed = 1.2;
            this.tweenTimer = 0;
            this.hasCrashed = false;
            this.DoMovement = function () {
                if (_this.curCell != null) {
                    if (_this.curCell.GetOccupyingTrack() != null) {
                        if (_this.targetPosition == null) {
                            _this.LoadTrackMoveTween();
                        }
                        //If we've reached the target point, move to the next grid
                        if (_this.position.distance(_this.targetPosition) < Train.DISTANCE_TILL_TRACK_CHANGE) {
                            _this.currentOrientation = _this.curCell.GetOccupyingTrack().GetConnectionIndex(_this.ApproachingFromOrientation());
                            if (_this.GetCellAlongConnection(_this.currentOrientation) == null) {
                                _this.CrashTrain();
                                return;
                            }
                            if (_this.GetCellAlongConnection(_this.currentOrientation).GetOccupyingTrack() == null) {
                                _this.CrashTrain();
                                return;
                            }
                            if (_this.GetCellAlongConnection(_this.currentOrientation).GetOccupyingTrack().GetConnectionIndex(_this.ApproachingFromOrientation()) < 0) {
                                _this.CrashTrain();
                                return;
                            }
                            else {
                                _this.curCell = _this.GetCellAlongConnection(_this.currentOrientation);
                                _this.LoadTrackMoveTween();
                            }
                        }
                        //Execute the track tween
                        var outOfPause = 0;
                        _this.tweenTimer += _this.trainMoveSpeed * (_this.game.time.physicsElapsedMS / 1000);
                        if (_this.tweenTimer > 1) {
                            _this.tweenTimer = 1;
                        }
                        _this.position.x = Phaser.Math.bezierInterpolation(_this.currentTweenPtsX, _this.tweenTimer);
                        _this.position.y = Phaser.Math.bezierInterpolation(_this.currentTweenPtsY, _this.tweenTimer);
                        //Figure out the angle from position + last position
                        var movementVec = new Phaser.Point(_this.position.y - _this.lastPosition.y, _this.position.x - _this.lastPosition.x);
                        movementVec = movementVec.normalize();
                        _this.angle = -Math.atan2(movementVec.y, movementVec.x) * (180 / Math.PI);
                        _this.lastPosition.x = _this.position.x;
                        _this.lastPosition.y = _this.position.y;
                    }
                }
                //Deal with death and cleaning up the explosions
                if (_this.hasCrashed == true) {
                    var noOfDestroyed = 0;
                    if (_this.deathExplosions != null) {
                        for (var i = 0; i < _this.deathExplosions.length; i++) {
                            if (_this.deathExplosions[i] != null) {
                                if (_this.deathExplosions[i].animations.currentAnim != null) {
                                    if (_this.deathExplosions[i].animations.currentAnim.isFinished) {
                                        _this.deathExplosions[i].destroy(true);
                                        _this.game.stage.removeChild(_this.deathExplosions[i]);
                                    }
                                }
                                else {
                                    noOfDestroyed++;
                                }
                            }
                            else {
                                noOfDestroyed++;
                            }
                        }
                        if (noOfDestroyed >= _this.deathExplosions.length) {
                            console.log("Destroying train");
                            _this.game.stage.removeChild(_this);
                            _this.destroy(true);
                            return;
                        }
                    }
                }
            };
            //used for finding the next cell to go to after we've gone down this track 
            this.GetCellAlongConnection = function (connectionIndex) {
                if (connectionIndex == 0) {
                    return _this.curCell.GetNorthCell();
                }
                else if (connectionIndex == 1) {
                    return _this.curCell.GetEastCell();
                }
                else if (connectionIndex == 2) {
                    return _this.curCell.GetSouthCell();
                }
                else if (connectionIndex == 3) {
                    return _this.curCell.GetWestCell();
                }
                else {
                    throw new Error("Incorrect Orientation. GetCellAlongConnection(). Train.ts");
                    return null;
                }
            };
            //Essentially reverses the orientation, so we can get the approaching direction
            this.ApproachingFromOrientation = function () {
                _this.currentOrientation = Math.floor(_this.currentOrientation);
                if (_this.currentOrientation == 0) {
                    return 2;
                }
                else if (_this.currentOrientation == 1) {
                    return 3;
                }
                else if (_this.currentOrientation == 2) {
                    return 0;
                }
                else if (_this.currentOrientation == 3) {
                    return 1;
                }
                else {
                    throw new Error("Incorrect Orientation. ApproachingFromOrientation(). Train.ts");
                }
            };
            //Called when we hit another train or go onto a cell without a track
            this.CrashTrain = function () {
                if (_this.hasCrashed == false) {
                    _this.deathExplosions = [];
                    _this.hasCrashed = true;
                    _this.SpawnExplosion(); //Spawn one explosion so we dont just kill this thing immediately
                    _this.curCell = null;
                    var explosionOffsetMaxLength = 30;
                    var explosionNo = _this.game.rnd.integerInRange(4, 6);
                    for (var i = 0; i < explosionNo; i++) {
                        _this.game.time.events.add(Phaser.Timer.SECOND * _this.game.rnd.realInRange(0.0, 0.6), _this.SpawnExplosion, _this);
                    }
                    _this.game.time.events.add(Phaser.Timer.SECOND * 0.5, _this.SetInvisible, _this);
                }
            };
            this.SpawnExplosion = function () {
                var explosionOffsetMaxLength = 32;
                var explosion = _this.game.add.sprite(_this.x, _this.y, TrainBox.Assets.ExplosionAnim.assetKey);
                explosion.anchor = new Phaser.Point(0.5, 0.5);
                explosion.position.x = _this.x + _this.game.rnd.realInRange(-explosionOffsetMaxLength, explosionOffsetMaxLength);
                explosion.position.y = _this.y + _this.game.rnd.realInRange(-explosionOffsetMaxLength, explosionOffsetMaxLength);
                _this.deathExplosions.push(explosion);
                _this.deathExplosions[_this.deathExplosions.length - 1].animations.add('explode');
                _this.deathExplosions[_this.deathExplosions.length - 1].play('explode', 12, false);
                _this.game.sound.play(TrainBox.Assets.ExplosionAudio.assetKey);
            };
            this.SetInvisible = function () {
                _this.visible = false;
            };
            this.targetPosition = startPos;
            this.prevTargetPosition = startPos;
            this.curCell = startCell;
            this.anchor = new Phaser.Point(0.5, 0.5);
            this.currentTweenPtsX = [];
            this.currentTweenPtsY = [];
            this.lastPosition = startPos;
        }
        Train.prototype.LoadTrackMoveTween = function () {
            while (this.currentTweenPtsX.length > 0) {
                this.currentTweenPtsX.pop();
            }
            while (this.currentTweenPtsY.length > 0) {
                this.currentTweenPtsY.pop();
            }
            this.prevTargetPosition = new Phaser.Point(this.targetPosition.x, this.targetPosition.y);
            this.targetPosition = this.curCell.GetEdgePoint(this.curCell.GetOccupyingTrack().GetConnectionIndex(this.ApproachingFromOrientation()));
            this.currentTweenPtsX.push(this.prevTargetPosition.x);
            this.currentTweenPtsX.push(this.curCell.position.x + TrainBox.Grid.CELLXPIXELS / 2);
            this.currentTweenPtsX.push(this.targetPosition.x);
            this.currentTweenPtsY.push(this.prevTargetPosition.y);
            this.currentTweenPtsY.push(this.curCell.position.y + TrainBox.Grid.CELLYPIXELS / 2);
            this.currentTweenPtsY.push(this.targetPosition.y);
            this.tweenTimer = 0;
        };
        Train.DISTANCE_TILL_TRACK_CHANGE = 0.5; //How far from the end track point we can get before switching track
        return Train;
    })(Phaser.Sprite);
    TrainBox.Train = Train;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    //A single cell on the grid, can hold a reference to a track.
    var Cell = (function () {
        function Cell(_game, _parentGrid, _gridIndex, _worldPosition) {
            var _this = this;
            this.occupyingTrack = null;
            this.GetNorthCell = function () {
                return _this.parentGrid.GetCellAtIndex(new Phaser.Point(_this.gridIndex.x, _this.gridIndex.y - 1));
            };
            this.GetEastCell = function () {
                return _this.parentGrid.GetCellAtIndex(new Phaser.Point(_this.gridIndex.x + 1, _this.gridIndex.y));
            };
            this.GetSouthCell = function () {
                return _this.parentGrid.GetCellAtIndex(new Phaser.Point(_this.gridIndex.x, _this.gridIndex.y + 1));
            };
            this.GetWestCell = function () {
                return _this.parentGrid.GetCellAtIndex(new Phaser.Point(_this.gridIndex.x - 1, _this.gridIndex.y));
            };
            //Returns true if we've placed a track
            this.PlaceTrack = function (track, trackRotIndex) {
                // returns out if the placement is unnescesary
                if (_this.occupyingTrack != null) {
                    if (_this.occupyingTrack.GetTrackType() == track.GetTrackType()) {
                        if (_this.occupyingTrack.GetTrackRotation() == trackRotIndex) {
                            return false;
                        }
                    }
                }
                if (_this.occupyingTrack != null) {
                    _this.game.stage.removeChild(_this.occupyingTrack);
                    _this.occupyingTrack.destroy();
                    _this.occupyingTrack = null;
                }
                var trackToAdd = new TrainBox.Track(_this.game, track.GetTrackType(), trackRotIndex, true);
                _this.occupyingTrack = _this.game.world.addAt(trackToAdd, 5);
                _this.occupyingTrack.x = _this.position.x + TrainBox.Grid.CELLXPIXELS / 2;
                _this.occupyingTrack.y = _this.position.y + TrainBox.Grid.CELLYPIXELS / 2;
                return true;
            };
            //returns true if we've deleted a track
            this.EraseTrack = function () {
                console.log('erasing');
                // returns out if the erasing is unnescesary
                if (_this.occupyingTrack == null) {
                    return false;
                }
                console.log("Erasinghere");
                if (_this.occupyingTrack != null) {
                    _this.game.stage.removeChild(_this.occupyingTrack);
                    _this.occupyingTrack.destroy();
                    _this.occupyingTrack = null;
                }
                return true;
            };
            this.GetOccupyingTrack = function () {
                return _this.occupyingTrack;
            };
            //Returns the position in the center of one edge, as defined by direction (0-3, N-W)
            this.GetEdgePoint = function (direction) {
                direction = Math.floor(direction);
                if (direction < 0) {
                    direction = 0;
                }
                if (direction > 3) {
                    direction = 3;
                }
                if (direction == 0) {
                    return new Phaser.Point(_this.position.x + TrainBox.Grid.CELLXPIXELS / 2, _this.position.y);
                }
                else if (direction == 1) {
                    return new Phaser.Point(_this.position.x + TrainBox.Grid.CELLXPIXELS, _this.position.y + TrainBox.Grid.CELLYPIXELS / 2);
                }
                else if (direction == 2) {
                    return new Phaser.Point(_this.position.x + TrainBox.Grid.CELLXPIXELS / 2, _this.position.y + TrainBox.Grid.CELLYPIXELS);
                }
                else if (direction == 3) {
                    return new Phaser.Point(_this.position.x, _this.position.y + TrainBox.Grid.CELLYPIXELS / 2);
                }
                else {
                    throw new Error("Incorrect Direction. GetEdgePoint(), Cell.ts");
                }
            };
            this.parentGrid = _parentGrid;
            this.gridIndex = _gridIndex;
            this.position = _worldPosition;
            this.game = _game;
        }
        return Cell;
    })();
    TrainBox.Cell = Cell;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    //The square grid, contains an array of cells and accesor methods for um ... accesing them.
    var Grid = (function () {
        function Grid(game, xGridWidth, yGridWidth) {
            var _this = this;
            //Called from constructor, so we can bind 'this' reference properly
            this.PopulateGridWithCells = function (game, xGridWidth, yGridWidth) {
                _this.cells = [];
                //The provided x and y numbers are rounded so they're ints, as javascript dosen't "do" ints, the high maintenance bitch.
                xGridWidth = Math.round(xGridWidth);
                yGridWidth = Math.round(yGridWidth);
                //Shove the cells in, creating Y-arrays as we go. Cells are given reference to this, and we use the CELLX/YPIXEL constants to hook up cell positions
                for (var i = 0; i < xGridWidth; i++) {
                    _this.cells[i] = [];
                    for (var j = 0; j < yGridWidth; j++) {
                        var newCell = new TrainBox.Cell(game, _this, new Phaser.Point(i, j), new Phaser.Point(i * Grid.CELLXPIXELS, j * Grid.CELLYPIXELS));
                        _this.cells[i].push(newCell);
                    }
                }
            };
            //Directly access cell at specified index
            this.GetCellAtIndex = function (index) {
                //Round numbers to ints
                index = index.floor();
                if (_this.cells.length > index.x) {
                    if (_this.cells[index.x].length > index.y) {
                        return _this.cells[index.x][index.y];
                    }
                }
                //If we got here, we tried to access out of array bounds, and should error.
                //throw new Error("Attempting To Access out of range Cell. Grid.ts, GetCellAtIndex()");
                return null;
            };
            //Access the cell under a world position, most useful for getting a cell at the mouse position I'd imagine, although if theres camera translation screen->worldspace will need to be done.
            this.GetCellAtWorldPoint = function (worldPosition) {
                //Derive x/y indices from world positions
                var xIndex = worldPosition.x / Grid.CELLXPIXELS;
                var yIndex = worldPosition.y / Grid.CELLYPIXELS;
                //Round the indices down.
                xIndex = Math.floor(xIndex);
                yIndex = Math.floor(yIndex);
                if (_this.cells.length > xIndex) {
                    if (_this.cells[xIndex].length > yIndex) {
                        return _this.cells[xIndex][yIndex];
                    }
                }
                //If we got here, we tried to access out of array bounds, and should error.
                throw new Error("Attempting To Access out of range Cell. Grid.ts, GetCellAtWorldPoint()");
                return null;
            };
            this.PopulateGridWithCells(game, xGridWidth, yGridWidth);
        }
        //The width/height of the cells, in pixels (hopefully).
        Grid.CELLXPIXELS = 110;
        Grid.CELLYPIXELS = 110;
        return Grid;
    })();
    TrainBox.Grid = Grid;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    //A wrapper around the camera controls, so we can limit them if we want, and have controls
    var Camera = (function () {
        //Pass in start positions
        function Camera(game, startX, startY, stageWidth, stageHeight) {
            var _this = this;
            this.camMoveSpeed = 0.3; //How fast the camera moves aboot'
            this.isScrolling = false; //Whether or not the camera is moving
            this.Update = function (deltaTime) {
                _this.isScrolling = false;
                //Move camera according to keyboard controls
                //Up, W and UpArrow
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.W)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.UP))) {
                    _this.gameRef.camera.y -= _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
                //Down, S and DownArrow
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.S)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.DOWN))) {
                    _this.gameRef.camera.y += _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
                //Up, A and LeftArrow
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.A)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.LEFT))) {
                    _this.gameRef.camera.x -= _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
                //Up, D and RightArrow
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.D)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.RIGHT))) {
                    _this.gameRef.camera.x += _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
            };
            this.ScreenToWorldSpace = function (screenPos) {
                var worldPos = new Phaser.Point(screenPos.x, screenPos.y);
                worldPos.x += _this.gameRef.camera.x;
                worldPos.y += _this.gameRef.camera.y;
                return worldPos;
            };
            this.IsScrolling = function () {
                return _this.isScrolling;
            };
            game.camera.setPosition(startX * 2, startY * 2);
            game.camera.bounds = new Phaser.Rectangle(startX * 2, startY * 2, stageWidth, stageHeight);
            this.gameRef = game;
        }
        return Camera;
    })();
    TrainBox.Camera = Camera;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    //The cursor that highlights what cell we have selected
    var Cursor = (function (_super) {
        __extends(Cursor, _super);
        function Cursor(game, startSelectingCell, gameGrid) {
            var _this = this;
            _super.call(this, game, startSelectingCell.position.x, startSelectingCell.position.y, TrainBox.Assets.CrossHair.assetKey);
            this.TWEEN_TO_CELL_SPEED = 80;
            this.isTweening = false;
            this.currentRot = 0; // 0-3, north to west
            this.isActive = true;
            //Sets the reference to the selection panel, seperate to avoid initialisation ordering issues cause they both need to know about eachother
            this.SetSelectionPanelReference = function (_panel) {
                _this.selectionPanel = _panel;
            };
            this.SnapToSelectingCell = function (cell) {
                _this.position = cell.position;
                _this.currentSelectedCell = cell;
            };
            this.TweenToSelectingCell = function (cell) {
                _this.isTweening = true;
                _this.currentSelectedCell = cell;
                _this.moveTween.to({ x: cell.position.x, y: cell.position.y }, _this.TWEEN_TO_CELL_SPEED, Phaser.Easing.Quadratic.Out, true, 0);
                _this.moveTween.onComplete.add(_this.tweenComplete, _this);
                _this.moveTween.start();
            };
            this.Enable = function () {
                _this.isActive = true;
                _this.visible = true;
                _this.trackSelectionGhost.visible = true;
            };
            this.Disable = function () {
                _this.isActive = false;
                _this.visible = false;
                _this.trackSelectionGhost.visible = false;
            };
            this.Update = function (mousePos, cam) {
                var selectedCell = _this.gridReference.GetCellAtWorldPoint(new Phaser.Point(mousePos.x, mousePos.y));
                //this.DoTweenCursorMovement();
                _this.SnapToSelectingCell(selectedCell);
                //If the camera is moving, don't display this sprite
                if (cam.IsScrolling()) {
                    _this.Disable();
                }
                else {
                    _this.Enable();
                }
                if (_this.deleteKey.isDown) {
                    _this.EraseTrack();
                }
                _this.RenderPlacementGhost();
            };
            this.EraseTrack = function () {
                if (_this.currentSelectedCell != null) {
                    if (_this.selectionPanel.GetSelectedItem() != null) {
                        //If this track can't be modified, bail out
                        if (_this.currentSelectedCell.GetOccupyingTrack() != null) {
                            if (_this.currentSelectedCell.GetOccupyingTrack().CanBeModified() == false) {
                                return;
                            }
                        }
                        if (_this.currentSelectedCell.EraseTrack()) {
                            _this.placeRailSound = _this.game.sound.play(TrainBox.Assets.PlaceTrackAudio.assetKey);
                        }
                    }
                }
            };
            //We've clicked, so place a track if we have one.
            this.PlaceTrack = function () {
                if (_this.game.input.activePointer.isDown) {
                    if (_this.game.input.activePointer.button == Phaser.Mouse.LEFT_BUTTON) {
                        if (_this.currentSelectedCell != null) {
                            if (_this.selectionPanel.GetSelectedItem() != null) {
                                //If this track can't be modified, bail out
                                if (_this.currentSelectedCell.GetOccupyingTrack() != null) {
                                    if (_this.currentSelectedCell.GetOccupyingTrack().CanBeModified() == false) {
                                        return;
                                    }
                                }
                                if (_this.currentSelectedCell.PlaceTrack(_this.selectionPanel.GetSelectedItem().track, _this.currentRot)) {
                                    _this.placeRailSound = _this.game.sound.play(TrainBox.Assets.PlaceTrackAudio.assetKey);
                                }
                            }
                        }
                    }
                }
            };
            this.RotateTrack = function () {
                if (_this.game.input.activePointer.isDown) {
                    if (_this.game.input.activePointer.button == Phaser.Mouse.RIGHT_BUTTON) {
                        if (_this.currentSelectedCell != null) {
                            if (_this.selectionPanel.GetSelectedItem() != null) {
                                _this.currentRot += 1.0;
                                _this.currentRot = Math.floor(_this.currentRot);
                                if (_this.currentRot > 3) {
                                    _this.currentRot = 0;
                                }
                                _this.trackSelectionGhost.rotation = _this.currentRot * Math.PI / 2;
                            }
                        }
                    }
                }
            };
            //Renders the track to be placed over the cursor
            this.RenderPlacementGhost = function () {
                if (_this.selectionPanel != null) {
                    if (_this.selectionPanel.GetSelectedItem() != null) {
                        if (_this.selectionPanel.GetSelectedItem().track.GetSmallKey() != _this.trackSelectionGhost.key) {
                            if (_this.selectionPanel.GetSelectedItem().track.GetSmallKey() != null) {
                                _this.trackSelectionGhost.loadTexture(_this.selectionPanel.GetSelectedItem().track.GetSmallKey());
                                _this.trackSelectionGhost.alpha = 0.7;
                                _this.trackSelectionGhost.visible = true;
                            }
                            else {
                                _this.trackSelectionGhost.visible = false;
                            }
                        }
                        _this.trackSelectionGhost.position.x = _this.position.x + _this.width / 2;
                        _this.trackSelectionGhost.position.y = _this.position.y + _this.height / 2;
                    }
                }
            };
            //Tweens along the grid, keeping snapped to the grid, rather than moving directly to the cursor. Pretty shitty with mouse movement, would be good for keyboard movement though, if you slowed it a bit.
            this.DoTweenCursorMovement = function (selectedCell) {
                //Tween movement, not great at the moment
                if (_this.isTweening == false) {
                    if (selectedCell != _this.currentSelectedCell) {
                        _this.moveTween = _this.game.add.tween(_this);
                        //tween horizontal
                        if (Math.abs(selectedCell.gridIndex.x - _this.currentSelectedCell.gridIndex.x) >= (Math.abs(selectedCell.gridIndex.y - _this.currentSelectedCell.gridIndex.y))) {
                            if (selectedCell.gridIndex.x < _this.currentSelectedCell.gridIndex.x) {
                                _this.TweenToSelectingCell(_this.currentSelectedCell.GetWestCell());
                            }
                            else {
                                _this.TweenToSelectingCell(_this.currentSelectedCell.GetEastCell()); //Tween rigth
                            }
                        }
                        else {
                            if (selectedCell.gridIndex.y < _this.currentSelectedCell.gridIndex.y) {
                                _this.TweenToSelectingCell(_this.currentSelectedCell.GetNorthCell());
                            }
                            else {
                                _this.TweenToSelectingCell(_this.currentSelectedCell.GetSouthCell()); //Tween south
                            }
                        }
                    }
                }
            };
            this.currentSelectedCell = startSelectingCell;
            this.gridReference = gameGrid;
            this.tweenComplete = function () {
                this.isTweening = false;
            };
            this.trackSelectionGhost = this.game.add.sprite(0, 0);
            this.trackSelectionGhost.anchor = new Phaser.Point(0.5, 0.5);
            this.inputEnabled = true;
            this.events.onInputDown.add(this.PlaceTrack, this);
            this.events.onInputOver.add(this.PlaceTrack, this);
            this.events.onInputDown.add(this.RotateTrack, this);
            this.deleteKey = game.input.keyboard.addKey(Phaser.Keyboard.DELETE);
        }
        return Cursor;
    })(Phaser.Sprite);
    TrainBox.Cursor = Cursor;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    //Stores the tracks that can be selected, tied to the rectangle object that defines their selection area on the panel
    var PanelTrackSelectionItem = (function () {
        //Pass in a track object, and the sprite that you want to be the background for it in the selection panel
        function PanelTrackSelectionItem(game, _track, _panel, parentSelectionPanel) {
            var _this = this;
            this.isSelected = false;
            this.BringPanelToTop = function () {
                _this.panel.bringToTop();
                _this.track.bringToTop();
            };
            this.OnMouseOver = function () {
                if (_this.isSelected == false) {
                    _this.panel.loadTexture(TrainBox.Assets.PanelInsertHighlight.assetKey);
                }
                else {
                    _this.panel.loadTexture(TrainBox.Assets.PanelInsertGreySelected.assetKey);
                }
            };
            this.OnMouseOff = function () {
                if (_this.isSelected == false) {
                    _this.panel.loadTexture(TrainBox.Assets.PanelInsert.assetKey);
                }
                else {
                    _this.panel.loadTexture(TrainBox.Assets.PanelInsertGrey.assetKey);
                }
            };
            this.OnMouseClick = function () {
                _this.selectionPanel.NotifyOfSelection(_this);
                _this.isSelected = true;
                _this.panel.loadTexture(TrainBox.Assets.PanelInsertGreySelected.assetKey);
            };
            this.Deselect = function () {
                _this.isSelected = false;
                _this.panel.loadTexture(TrainBox.Assets.PanelInsert.assetKey);
            };
            this.SetPosition = function (position) {
                console.log(position);
                _this.panel.cameraOffset.x = position.x;
                _this.panel.cameraOffset.y = position.y;
                _this.track.cameraOffset.x = position.x;
                _this.track.cameraOffset.y = position.y;
                if ((_this.track.key == TrainBox.Assets.CurveTrack1Large.assetKey) || (_this.track.key == TrainBox.Assets.CurveTrack1.assetKey)) {
                    _this.track.cameraOffset.x -= _this.track.width / 8;
                    _this.track.cameraOffset.y -= _this.track.height / 8;
                }
            };
            this.TweenToX = function (game, position, tweenSpeed) {
                game.add.tween(_this.panel.cameraOffset).to({ x: position }, tweenSpeed, Phaser.Easing.Back.InOut, true);
                if ((_this.track.key == TrainBox.Assets.CurveTrack1Large.assetKey) || (_this.track.key == TrainBox.Assets.CurveTrack1.assetKey)) {
                    game.add.tween(_this.track.cameraOffset).to({ x: position - (_this.track.width / 8) }, tweenSpeed, Phaser.Easing.Back.InOut, true);
                }
                else {
                    game.add.tween(_this.track.cameraOffset).to({ x: position }, tweenSpeed, Phaser.Easing.Back.InOut, true);
                }
            };
            this.track = _track;
            this.panel = _panel;
            this.track.fixedToCamera = true;
            this.panel.fixedToCamera = true;
            this.track.anchor = new Phaser.Point(0.5, 0.5);
            this.panel.anchor = new Phaser.Point(0.5, 0.5);
            this.panel.bringToTop();
            this.track.bringToTop();
            this.panel = game.add.existing(this.panel);
            this.track.loadTexture(this.track.GetSmallKey());
            this.track = game.add.existing(this.track);
            this.panel.inputEnabled = true;
            this.panel.events.onInputOver.add(this.OnMouseOver, this);
            this.panel.events.onInputOut.add(this.OnMouseOff, this);
            this.panel.events.onInputDown.add(this.OnMouseClick, this);
            this.selectionPanel = parentSelectionPanel;
        }
        PanelTrackSelectionItem.PANELXOFFSET = 82;
        PanelTrackSelectionItem.PANELYOFFSET = 70;
        PanelTrackSelectionItem.PANELYSPACING = 125;
        return PanelTrackSelectionItem;
    })();
    TrainBox.PanelTrackSelectionItem = PanelTrackSelectionItem;
    //The Panel that appears on the left side of the screen. Can be slid on and of the screen by hitting the selection area
    //Allows selection of which piece is active for placement/whatever tool we have
    var SelectionPanel = (function (_super) {
        __extends(SelectionPanel, _super);
        //Can start either open or closed.
        function SelectionPanel(game, startClosed, _cursor) {
            var _this = this;
            _super.call(this, game, 0, 0, TrainBox.Assets.SidePanel.assetKey);
            //Whether or not the panel is open
            this.isOpen = true;
            //Length of the tween
            this.tweenSpeed = 520;
            this.isCurrentlyTweening = false;
            //At the moment the button strip is 30 pixels on the right of the sprite
            this.buttonStripWidth = 44;
            this.panelOffset = -190; //The X offset of the panel, as the tween| lets you see a piece of the sprite that normally sits in -x.
            this.selectedItem = null; //The current selected button
            this.SpawnNewTrain = function () {
            };
            //Disable the cursor when over this panel, and enable when not
            this.DisableCursor = function () {
                _this.cursor.Disable();
            };
            this.EnableCursor = function () {
                _this.cursor.Enable();
            };
            this.BringPanelToTop = function () {
                _this.bringToTop();
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].BringPanelToTop();
                }
                _this.trainButton.bringToTop();
            };
            //Place the track buttons in the panel
            this.PopulateSelectableTracks = function (game, trainButtonCallBack) {
                _this.selectableTracks.push(new PanelTrackSelectionItem(game, new TrainBox.Track(game, TrainBox.TrackType.STRAIT), new Phaser.Sprite(_this.game, 0, 0, TrainBox.Assets.PanelInsert.assetKey), _this));
                _this.selectableTracks.push(new PanelTrackSelectionItem(game, new TrainBox.Track(game, TrainBox.TrackType.CORNER), new Phaser.Sprite(_this.game, 0, 0, TrainBox.Assets.PanelInsert.assetKey), _this));
                _this.selectableTracks.push(new PanelTrackSelectionItem(game, new TrainBox.Track(game, TrainBox.TrackType.CROSS), new Phaser.Sprite(_this.game, 0, 0, TrainBox.Assets.PanelInsert.assetKey), _this));
                _this.selectableTracks.push(new PanelTrackSelectionItem(game, new TrainBox.Track(game, TrainBox.TrackType.LRIGHT), new Phaser.Sprite(_this.game, 0, 0, TrainBox.Assets.PanelInsert.assetKey), _this));
                _this.selectableTracks.push(new PanelTrackSelectionItem(game, new TrainBox.Track(game, TrainBox.TrackType.LLEFT), new Phaser.Sprite(_this.game, 0, 0, TrainBox.Assets.PanelInsert.assetKey), _this));
                _this.trainButton = _this.game.add.button(SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT, _this.game.height - 150, TrainBox.Assets.MakeTrainButton.assetKey, trainButtonCallBack, _this, 0, 2, 1, 3);
                _this.trainButton.fixedToCamera = true;
                _this.trainButton.cameraOffset.x = SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT - _this.width + _this.buttonStripWidth + _this.trainButton.width;
                //add all the track sprites
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].SetPosition(new Phaser.Point(PanelTrackSelectionItem.PANELXOFFSET, PanelTrackSelectionItem.PANELYOFFSET + (i * PanelTrackSelectionItem.PANELYSPACING)));
                }
                if (_this.isOpen) {
                    _this.SetDirectlyOpen();
                }
                else {
                    _this.SetDirectlyClosed();
                }
            };
            this.Update = function (mousePos) {
                if (_this.game.input.mousePointer.isDown) {
                    if (_this.selectionArea.contains(mousePos.x, mousePos.y)) {
                        _this.Toggle();
                    }
                }
            };
            //If the panel is open, shuts it, and vice versa
            this.Toggle = function () {
                if (_this.isCurrentlyTweening == false) {
                    //Tween panel off the left side of the screen
                    if (_this.isOpen) {
                        _this.TweenOut();
                    }
                    else {
                        _this.TweenIn();
                    }
                }
            };
            //Sets the panel to be directly open, without tweens or somesuch
            this.SetDirectlyOpen = function () {
                _this.isCurrentlyTweening = false;
                _this.selectionArea.x = _this.width - _this.buttonStripWidth + _this.panelOffset;
                _this.isOpen = true;
                _this.cameraOffset.x = _this.panelOffset;
                //Set the panels to the open position
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].SetPosition(new Phaser.Point(PanelTrackSelectionItem.PANELXOFFSET, PanelTrackSelectionItem.PANELYOFFSET + (i * PanelTrackSelectionItem.PANELYSPACING)));
                }
                if (_this.trainButton != null) {
                    _this.trainButton.cameraOffset = new Phaser.Point(SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT, _this.game.height - SelectionPanel.TRAINBUTTONYFROMBOTTOM - (_this.trainButton.height / 2));
                }
            };
            //Sets the panel to be directly closed, without tweens or somesuch
            this.SetDirectlyClosed = function () {
                _this.isCurrentlyTweening = false;
                _this.isOpen = false;
                _this.selectionArea.x = 0;
                _this.cameraOffset.x = -_this.width + _this.buttonStripWidth;
                //Set the panels to the closed position
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].SetPosition(new Phaser.Point(PanelTrackSelectionItem.PANELXOFFSET - _this.width + _this.buttonStripWidth + _this.selectableTracks[i].panel.width, PanelTrackSelectionItem.PANELYOFFSET + (i * PanelTrackSelectionItem.PANELYSPACING)));
                }
                if (_this.trainButton != null) {
                    _this.trainButton.position = new Phaser.Point(SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT - _this.width + _this.buttonStripWidth + _this.trainButton.width, _this.game.height - SelectionPanel.TRAINBUTTONYFROMBOTTOM);
                }
            };
            //Tween out left, only called internally
            this.TweenOut = function () {
                _this.game.add.tween(_this.cameraOffset).to({ x: 0 - _this.width + _this.buttonStripWidth }, _this.tweenSpeed, Phaser.Easing.Back.InOut, true).onComplete.add(_this.TweenOutComplete, _this);
                _this.isCurrentlyTweening = true;
                _this.slideAudio = _this.game.sound.play(TrainBox.Assets.SlideAudio.assetKey);
                //tween the panels
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].TweenToX(_this.game, PanelTrackSelectionItem.PANELXOFFSET - _this.width + _this.buttonStripWidth + _this.selectableTracks[i].panel.width, _this.tweenSpeed);
                }
                _this.game.add.tween(_this.trainButton.cameraOffset).to({
                    x: 0 - (_this.trainButton.width * 2) + _this.buttonStripWidth
                }, _this.tweenSpeed, Phaser.Easing.Back.InOut, true);
            };
            //Tween In from left, only called internally
            this.TweenIn = function () {
                _this.game.add.tween(_this.cameraOffset).to({ x: 0 + _this.panelOffset }, _this.tweenSpeed, Phaser.Easing.Back.InOut, true).onComplete.add(_this.TweenInComplete, _this);
                _this.isCurrentlyTweening = true;
                _this.slideAudio = _this.game.sound.play(TrainBox.Assets.SlideAudio.assetKey);
                //tween the panels
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].TweenToX(_this.game, PanelTrackSelectionItem.PANELXOFFSET, _this.tweenSpeed);
                }
                _this.game.add.tween(_this.trainButton.cameraOffset).to({
                    x: SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT
                }, _this.tweenSpeed, Phaser.Easing.Back.InOut, true);
            };
            //Move button to closed position and set tween flag,
            this.TweenOutComplete = function () {
                _this.isCurrentlyTweening = false;
                _this.isOpen = false;
                _this.selectionArea.x = 0;
            };
            //Move button to open position and set tween flag,
            this.TweenInComplete = function () {
                _this.isCurrentlyTweening = false;
                _this.selectionArea.x = _this.width - _this.buttonStripWidth + _this.panelOffset;
                _this.isOpen = true;
            };
            //Called from the selection objects, notifies of a button selection so all the other buttons can be de-selected
            //This notification is called first, so we can go ahead an de-select everything
            this.NotifyOfSelection = function (selectedButton) {
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    if (_this.selectableTracks[i].isSelected) {
                        _this.selectableTracks[i].Deselect();
                    }
                }
                _this.selectedItem = selectedButton;
            };
            this.GetSelectedItem = function () {
                return _this.selectedItem;
            };
            this.GetItem = function (index) {
                index = Math.floor(index);
                if (_this.selectableTracks == null) {
                    return null;
                }
                if ((index < 0) || (index >= _this.selectableTracks.length)) {
                    return null;
                }
                return _this.selectableTracks[index];
            };
            this.DisableTrainSpawnButtonForTime = function () {
                _this.trainButton.inputEnabled = false;
                _this.trainButton.alpha = 0.5;
                _this.game.time.events.add(Phaser.Timer.SECOND * SelectionPanel.SPAWNTRAINBUTTONCOOLDOWN, _this.EnableTrainButton, _this);
            };
            this.EnableTrainButton = function () {
                _this.trainButton.inputEnabled = true;
                _this.trainButton.alpha = 1.0;
            };
            this.x += this.panelOffset;
            this.selectionArea = new Phaser.Rectangle(this.width - this.buttonStripWidth + this.panelOffset, 0, this.buttonStripWidth, this.height);
            this.cursor = _cursor;
            this.selectableTracks = [];
            this.fixedToCamera = true; //This is a UI element, so fix it to camera
            if (startClosed) {
                this.SetDirectlyClosed();
            }
            this.inputEnabled = true;
            this.cursor = _cursor;
            this.events.onInputOver.add(this.DisableCursor, this);
            this.events.onInputOut.add(this.EnableCursor, this);
        }
        SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT = 19;
        SelectionPanel.TRAINBUTTONYFROMBOTTOM = 80;
        SelectionPanel.SPAWNTRAINBUTTONCOOLDOWN = 5;
        return SelectionPanel;
    })(Phaser.Sprite);
    TrainBox.SelectionPanel = SelectionPanel;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    var BootState = (function (_super) {
        __extends(BootState, _super);
        function BootState() {
            _super.apply(this, arguments);
        }
        BootState.prototype.preload = function () {
            TrainBox.Assets.LoadImage(this, TrainBox.Assets.LoadingBar);
        };
        BootState.prototype.create = function () {
            //Disable the right click context menu, cause we want to use right click for rotating
            this.game.canvas.oncontextmenu = function (e) { e.preventDefault(); };
            //  Unless you specifically need to support multitouch I would recommend setting this to 1
            this.input.maxPointers = 1;
            //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
            this.stage.disableVisibilityChange = false;
            if (this.game.device.desktop) {
            }
            else {
            }
            this.game.state.start('PreloaderState', true, false);
        };
        return BootState;
    })(Phaser.State);
    TrainBox.BootState = BootState;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    //Entry point, called from app.ts. Just for state setup
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, window.innerWidth, window.innerHeight, Phaser.AUTO, 'content', null);
            this.state.add('BootState', TrainBox.BootState, false);
            this.state.add('PreloaderState', TrainBox.PreloaderState, false);
            this.state.add('TrainState', TrainBox.TrainState, false);
            this.state.start('BootState');
        }
        return Game;
    })(Phaser.Game);
    TrainBox.Game = Game;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    var PreloaderState = (function (_super) {
        __extends(PreloaderState, _super);
        function PreloaderState() {
            _super.apply(this, arguments);
        }
        PreloaderState.prototype.preload = function () {
            //  Set-up our preloader sprite
            this.preloadBar = this.add.sprite(200, 250, TrainBox.Assets.LoadingBar.assetKey);
            this.load.setPreloadSprite(this.preloadBar);
            //  Load game assets here, like this : this.load.image(TrainBox.AssetPaths.Asset.AssetKey, TrainBox.AssetPaths.Asset.AssetKey);
            TrainBox.Assets.LoadAllAssets(this);
        };
        PreloaderState.prototype.create = function () {
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startGame, this);
        };
        PreloaderState.prototype.startGame = function () {
            this.game.state.start('TrainState', true, false);
        };
        return PreloaderState;
    })(Phaser.State);
    TrainBox.PreloaderState = PreloaderState;
})(TrainBox || (TrainBox = {}));
var TrainBox;
(function (TrainBox) {
    var TrainState = (function (_super) {
        __extends(TrainState, _super);
        function TrainState() {
            var _this = this;
            _super.apply(this, arguments);
            this.gamePaused = false;
            this.SpawnTrain = function () {
                _this.game.sound.play(TrainBox.Assets.TrainButtonAudio.assetKey);
                _this.selectionPanel.DisableTrainSpawnButtonForTime();
                var noOfCarraigesToSpawn = _this.game.rnd.integerInRange(2, 8);
                var spawnOffsetY = 30;
                var startCell = _this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 1));
                var testTrain = new TrainBox.Train(_this.game, TrainBox.Assets.Train.assetKey, startCell, new Phaser.Point(380, startCell.position.y - spawnOffsetY));
                var lastSpawnObject = testTrain;
                testTrain.angle = 90;
                _this.trains.push(testTrain);
                for (var i = 0; i < noOfCarraigesToSpawn; i++) {
                    var testCarriage = new TrainBox.Carraige(_this.game, TrainBox.Assets.Carriage.assetKey, lastSpawnObject, testTrain);
                    testCarriage.position = new Phaser.Point(testTrain.position.x, testTrain.position.y);
                    testCarriage.angle = 90;
                    _this.game.add.existing(testCarriage);
                    lastSpawnObject = testCarriage;
                    _this.carriages.push(testCarriage);
                }
                _this.game.add.existing(testTrain);
                _this.selectionPanel.BringPanelToTop();
            };
            this.PausedGame = function () {
                _this.gamePaused = true;
            };
            this.UnPausedGame = function () {
                _this.gamePaused = false;
                _this.game.time.elapsed = 0.166;
            };
        }
        TrainState.prototype.create = function () {
            var gridWidth = 36;
            var gridHeight = 36;
            this.constructionGrid = new TrainBox.Grid(this.game, gridWidth, gridHeight);
            //Set up 4 tiled backgrounds, 2x2
            this.background1 = this.add.sprite(0, 0, TrainBox.Assets.Background.assetKey);
            this.background2 = this.add.sprite(this.background1.width, 0, TrainBox.Assets.Background.assetKey);
            this.background3 = this.add.sprite(0, this.background1.height, TrainBox.Assets.Background.assetKey);
            this.background4 = this.add.sprite(this.background1.width, this.background1.height, TrainBox.Assets.Background.assetKey);
            //Set the background logic disabled for performance
            this.background1.body = null;
            this.background2.body = null;
            this.background3.body = null;
            this.background4.body = null;
            this.startTrackNotificationArrow = this.add.sprite(0, 0, TrainBox.Assets.Arrow.assetKey);
            this.cam = new TrainBox.Camera(this.game, TrainBox.Grid.CELLXPIXELS * 2, TrainBox.Grid.CELLYPIXELS * 2, (TrainBox.Grid.CELLXPIXELS * (gridWidth - 4)), TrainBox.Grid.CELLYPIXELS * (gridHeight - 4));
            this.gridSelectionCursor = new TrainBox.Cursor(this.game, this.constructionGrid.GetCellAtIndex(new Phaser.Point(5, 0)), this.constructionGrid);
            this.selectionPanel = new TrainBox.SelectionPanel(this.game, false, this.gridSelectionCursor);
            this.gridSelectionCursor.SetSelectionPanelReference(this.selectionPanel);
            this.game.add.existing(this.gridSelectionCursor);
            this.game.add.existing(this.selectionPanel);
            this.selectionPanel.PopulateSelectableTracks(this.game, this.SpawnTrain);
            this.mousePos = new Phaser.Point(0, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 0)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0); //Entry track
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 0)).GetOccupyingTrack().SetCannotBeModified(); //Dont modify the entry track   
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 1)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 1)).GetOccupyingTrack().SetCannotBeModified(); //Dont modify the entry track
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 2)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 2)).GetOccupyingTrack().SetCannotBeModified(); //Dont modify the entry track
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 3)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 3)).GetOccupyingTrack().SetCannotBeModified(); //Dont modify the entry track
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).GetOccupyingTrack().SetCannotBeModified(); //Dont modify the entry track
            this.startTrackNotificationArrow.position = new Phaser.Point(this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).position.x, this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).position.y); //Set a notification arrow;
            this.startTrackNotificationArrow.position.x -= 45;
            this.startTrackNotificationArrow.position.y += 15;
            this.trains = [];
            this.carriages = [];
            this.game.onPause.add(this.PausedGame, this);
            this.game.onResume.add(this.UnPausedGame, this);
        };
        TrainState.prototype.update = function () {
            if (this.gamePaused == false) {
                this.mousePos.x = this.input.x;
                this.mousePos.y = this.input.y;
                this.cam.Update(this.time.elapsedMS);
                this.gridSelectionCursor.Update(this.cam.ScreenToWorldSpace(this.mousePos), this.cam);
                this.selectionPanel.Update(this.mousePos);
                for (var i = 0; i < this.trains.length; i++) {
                    if (this.trains[i].alive != false) {
                        this.trains[i].DoMovement();
                    }
                    else {
                        delete this.trains[i];
                        this.trains.splice(i, 1);
                    }
                }
                for (var i = 0; i < this.carriages.length; i++) {
                    if (this.carriages[i].alive != false) {
                        this.carriages[i].Update();
                    }
                    else {
                        delete this.carriages[i];
                        this.carriages.splice(i, 1);
                    }
                }
                //Check collision between trains
                for (var i = 0; i < this.trains.length; i++) {
                    for (var j = 0; j < this.trains.length; j++) {
                        if ((this.trains[i] != null) && (this.trains[j] != null)) {
                            if ((this.trains[i].alive) && (this.trains[j].alive)) {
                                if (this.trains[i].overlap(this.trains[j])) {
                                    if (this.trains[i] != this.trains[j]) {
                                        this.trains[j].CrashTrain();
                                    }
                                }
                            }
                        }
                    }
                }
                //Check collision between carriages
                for (var i = 0; i < this.carriages.length; i++) {
                    for (var j = 0; j < this.trains.length; j++) {
                        if ((this.carriages[i] != null) && (this.trains[j] != null)) {
                            if ((this.carriages[i].alive) && (this.trains[j].alive)) {
                                if (this.carriages[i].overlap(this.trains[j])) {
                                    if (this.carriages[i].belongsToTrain != this.trains[j]) {
                                        this.trains[j].CrashTrain();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
        return TrainState;
    })(Phaser.State);
    TrainBox.TrainState = TrainState;
})(TrainBox || (TrainBox = {}));
//# sourceMappingURL=game.js.map